<?php
$l['number'] 						= 'REF No';
$l['number2'] 						= 'No';
$l['lponumber'] 					= 'LPO No';
$l['delnumber'] 						= 'Del No.';
$l['date'] 							= 'Dated';
$l['due'] 							= 'Delivery Date';
$l['to'] 							= 'Ship To';
$l['ship']							= 'Delivery To';
$l['from'] 							= 'Our information';
$l['product'] 						= 'Item Description';
$l['amount'] 						= 'Qty';
$l['price'] 						= 'Price';
$l['discount'] 						= 'Discount';
$l['vat'] 							= 'Tax/Vat';
$l['total'] 						= 'Total';
$l['sub_total'] 						= 'Sub Total';
$l['page'] 							= 'Page';
$l['page_of'] 						= 'of';
$l['si'] 						= '#';
$l['accounts'] 						= 'Mpesa No:0729547704 | Till No:825560 | Co-op A/C:0178221212';
$l['attention'] 						= 'Attention';
?>