<?php
$l['number'] 						= 'REF No';
$l['lponumber'] 					= 'LPO No';
$l['delnumber'] 						= 'Del No.';
$l['date'] 							= 'Date';
$l['due'] 							= 'Due date';
$l['to'] 							= 'To';
$l['ship']							= 'Delivery To';
$l['from'] 							= 'Our information';
$l['product'] 						= 'Item Description';
$l['amount'] 						= 'Qty';
$l['price'] 						= 'Price';
$l['discount'] 						= 'Discount';
$l['vat'] 							= 'Tax/Vat';
$l['total'] 						= 'Total';
$l['sub_total'] 						= 'Sub Total';
$l['page'] 							= 'Page';
$l['page_of'] 						= 'of';
$l['si'] 						= '#';
$l['accounts'] 						= 'Accounts are Due on 	Demand';
$l['attention'] 						= 'Attention';
?>