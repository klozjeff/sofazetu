<!DOCTYPE html>
<html>
<head>
    <title>Receipt</title>
</head>
<body>
    <table>
        <tr>
            <td colspan="3" align="center">  <img alt="" src="http://localhost/sofazetu/public/image/sofazetuLogo.png" style="height:70px;" /></td>
        </tr>
        <tr>

            <td colspan="3" align="center">P.0 Box 60596-00200</td>
        </tr>
        <tr>
            <td colspan="3" align="center">Nairobi-Kenya</td>
        </tr>
        <tr>
            <td colspan="3" align="center">0729547704/0702828402 <br/>0790795566</td>
        </tr>

        <tr>
            <td colspan="3" align="center">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="2"><?php echo date('Y-m-d',strtotime($sales_data['created_at']));?></td>
            <td align="right"><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['name'];?></td>
        </tr>
        <tr>
            <td colspan="2">#XIA/SF/<?php echo $sales_data['uniqueID'];?></td>
            <td align="right"><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['phone'];?></td>
        </tr>

        <tr>
            <td colspan="3" align="center">&nbsp;</td>
        </tr>
<?php
        foreach($sales_items as $item):
            echo '<tr>
                <td colspan="3">'.$this->crm_model->getDataById('products',$item['product_id'])['phone'].'</td>
            </tr>
            <tr>
                <td colspan="2">'.$item['quantity'].' x '.$item['price'].'</td>
                <td align="right">'.$item['sub_total'].'</td>
            </tr>';
       endforeach;
?>

        <tr>
            <td colspan="3" align="center">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="2" align="left">Subtotal</td>
            <td align="right"><?php echo $sales_data['sale_subtotal'];?></td>
        </tr>

        <tr>
            <td colspan="2" align="left">Delivery Fee</td>
            <td align="right"><?php echo $sales_data['sale_transport'];?></td>
        </tr>

        <tr>
            <td colspan="2" align="left">Total</td>
            <td align="right"><?php echo $sales_data['sale_total'];?></td>
        </tr>

        <tr>
            <td colspan="3" align="center">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="3" align="center">Thank you for choosing Sofazetu</td>

        </tr>
        <tr>

            <td colspan="3" align="center">Served By <?php echo $this->crm_model->getDataById('users',$sales_data['cashier_id'])['name'];?></td>
        </tr>
    </table>
</body>
</html>