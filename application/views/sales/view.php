<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 6:35 PM
 */
?>
<?php error_reporting(0);$array=explode(',',$sales_data['sale_accessories']); ?>
<form id="edit_sale" action="">
        <div class="col-md-8">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div id="response" class="alert alert-success" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <div class="message"></div>
                    </div>
                    <div class="form-group">
<div class="col-md-4">


    <div class="input-group " id="invo">
        <input type="hidden" class="form-control" name="salesdb_id" value="<?php echo $sales_data['id'];?>">
        <span class="input-group-addon">XIA/SF/</span>

        <input type="text" name="sale_id" id="sale_id" disabled class="form-control" readonly placeholder="Sale Number"  aria-describedby="sizing-addon1" value="<?= $sales_data['uniqueID'];?>">
        <span class="input-group-addon">/<?php echo date('Y');?></span>
    </div>
</div>

                        <div class="col-md-4">


                            <select name="sale_type" id="sale_type" class="form-control">
                                <option <?php if($sales_data['sale_type']=='Order'):echo 'selected'; endif;?> value="<?php if($sales_data['sale_type']=='Order'): echo $sales_data['sale_type']; else: echo 'Order'; endif;?>">Order</option>
                                <option <?php if($sales_data['sale_type']=='Ready Made'):echo 'selected'; endif;?> value="<?php if($sales_data['sale_type']=='Ready Made'):echo $sales_data['sale_type']; else: echo 'Ready Made';endif;?>">Ready Made</option>
                                <option <?php if($sales_data['sale_type']=='New Sale'):echo 'selected'; endif;?> value="<?php if($sales_data['sale_type']=='New Sale'):echo $sales_data['sale_type']; else: echo 'New Sale';endif;?>">New Sale</option>

                            </select>
                        </div>
                        <div class="col-md-4">


                            <div class="input-group date" id="sale_delivery_date">
                                <input type="text" class="form-control required delivery_date" name="delivery_date" placeholder="Delivery Date" value="<?= $sales_data['delivery_date'];?>" data-date-format="YYYY-MM-DD" />
                                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>
                <div class="panel-body">
                    <div class="row">
                        <!--
                        <div class="col-md-2">
                            <div class="list-group">
                                <a href="#" v-for="category in categories" class="list-group-item list-group-item-info" v-on:click="setSearchItemKey(category.id)" track-by="$index" >{{ category.name }}</a>
                            </div>
                        </div>
                        -->
                        <div class="col-md-12" style="padding: 25px !important;">
                          <!--  <input type="text" class="form-control" placeholder="Search for product" v-model="searchProductQuery.q" onkeyup="searchProduct">
                            <br />-->
                            <div class="row">
                                <table id="invoice_table" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            <a href="#" class="add-row"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                        </th>
                                        <th width="350px">
                                          Product or Service
                                        </th>
                                        <th>
                                          Qty/Set
                                        </th>
                                        <th>
                                         Price
                                        </th>

                                        <th>
                                         Sub Total
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($sales_items as $item):?>
                                    <tr>
                                        <td> <a href="#" class="delete-row"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>

                                        </td>
                                        <td>
                                            <div class="form-group form-group-sm  no-margin-bottom">
                                                <textarea type="text" rows='4' class="form-control form-group-sm item-input invoice_product required" name="pos_product[]" value="" placeholder="Enter product Name and / or description"><?php echo $item['product_id'];?></textarea>
                                                <p class="item-select">or <a href="#">Select a product</a></p>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="form-group form-group-sm no-margin-bottom">
                                                <input type="text" class="form-control invoice_product_qty calculate" name="pos_product_qty[]" value="<?php echo $item['quantity'];?>">
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="input-group input-group-sm  no-margin-bottom">
                                                <span class="input-group-addon"></span>
                                                <input type="text" class="form-control calculate invoice_product_price required" name="pos_product_price[]" aria-describedby="sizing-addon1" placeholder="0.00" value="<?php echo $item['price'];?>">
                                            </div>
                                        </td>

                                        <td class="text-right">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon"></span>
                                                <input type="text" class="form-control calculate-sub" name="pos_product_sub[]" id="invoice_product_sub" value="0.00" aria-describedby="sizing-addon1" disabled <?php echo $item['sub_total'];?>>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" id="accessories">

                    <div class="row"> <div class="col-xs-5">&nbsp;<span class="">Accessories & Additional Information<br/></span></div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-1">
                            Qty
                        </div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-1">
                           Price
                        </div>
                        <div class="col-xs-2">
                          &nbsp; &nbsp; Sub Total
                        </div>
                    </div>
                    <?php $accessories=array();
                    foreach ($sales_acc as $items):
                        $accessories[]=$items;

                    endforeach;
                    $key = array_search('scatters', $accessories[0]);
                    function searchKey($products, $field, $value)
                    {
                        foreach($products as $key => $product)
                        {
                            if ( $product[$field] === $value )
                                return $key;
                        }
                        return false;
                    }
                    if(searchKey($accessories,'product_id','comforters')!==false):
                   $com=searchKey($accessories,'product_id','comforters');
                    $avcomm=1;
                    else:
                     $avcomm=0;
                    endif;
                    if(searchKey($accessories,'product_id','scatters')!==false):
                        $sca=searchKey($accessories,'product_id','scatters');
                       $avsca=1;
                       else:
                        $avsca=0;
                    endif;
                    if(searchKey($accessories,'product_id','footrest')!==false):
                        $foo=searchKey($accessories,'product_id','footrest');
                    $avfoo=1;
                    else:
                        $avfoo=0;
                    endif;
                    if(searchKey($accessories,'product_id','others')!==false):
                        $oth=searchKey($accessories,'product_id','others');
                    $avoth=1;
                    else:
                        $avoth=0;
                    endif;
                    ?>

                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            1. Comforters
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" <?php if (in_array('comforters',$array)): echo 'checked'; endif;?> name="box[]" value="comforters" id="box1" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text"  value="<?php if($avcomm==1): echo $accessories[$com]['quantity']; endif;?>" id="access_qty_1" class="form-control access_qty_1 " name="pos_qty_comforters" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avcomm==1): echo $accessories[$com]['price']; endif;?>" id="access_price_1" class="form-control calculateaccess access_price_1" name="pos_price_comforters" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avcomm==1): echo $accessories[$com]['sub_total']; endif;?>" disabled="disabled" class="form-control calculateaccess invoice_access_price1" name="pos_subtotal_comforters" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            2. Sausage Scatters
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox"  <?php if (in_array('scatters',$array)): echo 'checked'; endif;?> id="box2" name="box[]"  value="scatters" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avsca==1): echo $accessories[$sca]['quantity']; endif;?>" id="access_qty_2" class="form-control access_qty_2 " name="pos_qty_scatters" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avsca==1): echo $accessories[$sca]['price']; endif;?>" id="access_price_2" class="form-control calculateaccess access_price_2 " name="pos_price_scatters" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" value="<?php if($avsca==1): echo $accessories[$sca]['sub_total']; endif;?>" id="invoice_access_price2" class="form-control calculateaccess invoice_access_price2" name="pos_subtotal_scatters" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            3. FootRest
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" id="box3"  <?php if (in_array('footrest',$array)): echo 'checked'; endif;?> name="box[]" value="footrest" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avfoo==1): echo $accessories[$foo]['quantity']; endif;?>" id="access_qty_3" class="form-control access_qty_3 " name="pos_qty_footrest" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avfoo==1): echo $accessories[$foo]['price']; endif;?>" id="access_price_3" class="form-control calculateaccess access_price_3 " name="pos_price_footrest" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avfoo==1): echo $accessories[$foo]['sub_total']; endif;?>" disabled="disabled" id="invoice_access_price3" class="form-control calculateaccess invoice_access_price3" name="pos_subtotal_footrest" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            4. Others
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" id="box4"  <?php if (in_array('others',$array)): echo 'checked'; endif;?> name="box[]" value="others" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text"  value="<?php if($avoth==1): echo $accessories[$oth]['quantity']; endif;?>" id="access_qty_4" class="form-control access_qty_4 " name="pos_qty_others" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text"  value="<?php if($avoth==1): echo $accessories[$oth]['price']; endif;?>" id="access_price_4" class="form-control calculateaccess access_price_4 " name="pos_price_others" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" value="<?php if($avoth==1): echo $accessories[$oth]['sub_total']; endif;?>" disabled="disabled" id="invoice_access_price4" class="form-control calculateaccess invoice_access_price4" name="pos_subtotal_others" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>

                </div>


            </div>

        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Sale Updates</div>
                <div class="panel-body">
                    <div class="form-group">

                        <label class="control-label">Sale Status</label>




                            <select name="sale_status" id="sale_status" class="form-control required">
                                <option value="">Select</option>
                                <option <?php if($sales_data['status']=='In Progress'):echo 'selected'; endif;?> value="<?php if($sales_data['status']=='In Progress'): echo $sales_data['status']; else: echo 'In Progress'; endif;?>">In Progress</option>
                                <option <?php if($sales_data['status']=='Completed'):echo 'selected'; endif;?> value="<?php if($sales_data['status']=='Completed'): echo $sales_data['status']; else: echo 'Completed'; endif;?>">Completed</option>
                                <option <?php if($sales_data['status']=='Cancelled'):echo 'selected'; endif;?> value="<?php if($sales_data['status']=='Cancelled'): echo $sales_data['status']; else: echo 'Cancelled';endif;?>">Cancelled</option>
                                <option <?php if($sales_data['status']=='Suspended'):echo 'selected'; endif;?> value="<?php if($sales_data['status']=='Suspended'):echo $sales_data['status'];else: echo 'Suspended'; endif;?>">Suspended</option>

                            </select>
<br/>
                        <div id="stages">
                            <label class="control-label">Stages</label>




                            <select name="stage" id="stage" class="form-control">
                                <option value="">Select</option>
                                <option <?php if($sales_data['stage']==1):echo 'selected'; endif;?> value="<?php if($sales_data['stage']==1): echo '1'; else: echo '1'; endif;?>">Frame Ready</option>
                                <option <?php if($sales_data['stage']==2):echo 'selected'; endif;?> value="<?php if($sales_data['stage']==2): echo '2';else:echo '2'; endif;?>">Foaming Ready</option>
                                <option <?php if($sales_data['stage']==3):echo 'selected'; endif;?> value="<?php if($sales_data['stage']==3): echo '3'; else:echo '3';endif;?>">Cushions,Pillows & Accessories</option>

                            </select>
                        </div>

                        <br/>
                        <div class="stages">
                            <label class="control-label">Comments/Reasons</label>




                            <textarea name="comments" id="comments" class="form-control">
<?php echo $sales_data['comments'];?>
                            </textarea>
                        </div>


                    </div>


                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>
                <div class="panel-body">
                    <div class="form-group">

                            <label class="control-label">Customer Name</label>
                        <input type="hidden" class="form-control" value="<?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['id'];?>" name="customer_id">

                            <input type="text" class="form-control required" value="<?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['name'];?>" name="customer_name">

                            <label class="control-label">Contacts</label>

                            <input type="text" class="form-control required" value="<?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['phone'];?>" name="phone">
                        <label class="control-label">Address</label>

                        <textarea type="text" rows="1" class="form-control required" name="address"><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['address'];?></textarea>

                    </div>


                </div>
            </div>
            <div id="pay" class="panel panel-default">
                <div class="panel-heading">Payment</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <strong class="shipping">Deposit:</strong>
                        </div>
                        <div class="col-xs-6">
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Kshs</span>
                                <input type="text" class="form-control deposit" id="pos_deposit" name="pos_deposit" aria-describedby="sizing-addon1" value="<?= $sales_data['sale_deposit'];?>" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>Bal:</strong>
                        </div>
                        <div class="col-xs-6">
                            <span class="pos-balance"><?= $sales_data['sale_balance']!=''?$sales_data['sale_balance']:'0:00';?></span>
                            <input type="hidden" name="pos_balance" value="<?= $sales_data['sale_balance'];?>" id="pos_balance">
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Sub Total:</strong>
                            </div>
                            <div class="col-xs-6">
                               <span class="invoice-sub-total"><?= $sales_data['sale_subtotal']!=''?$sales_data['sale_subtotal']:'0:00';?></span>
                                <input type="hidden" value="<?= $sales_data['sale_subtotal'];?>" name="invoice_subtotal" id="invoice_subtotal">
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>Add. Accessories:</strong>
                        </div>
                        <div class="col-xs-6">
                            <span class="invoice-additional"><?= $sales_data['sale_accessories_total']!=''?$sales_data['sale_accessories_total']:'0:00';?></span>
                            <input type="hidden" value="<?= $sales_data['sale_accessories_total'];?>" name="invoice_additional" id="invoice_additional">
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <strong class="shipping">Delivery Cost:</strong>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">Kshs</span>
                                    <input type="text" class="form-control calculate shipping" name="invoice_shipping" aria-describedby="sizing-addon1" value="<?= $sales_data['sale_transport'];?>" placeholder="0.00">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Total:</strong>
                            </div>
                            <div class="col-xs-6">
                                <span class="invoice-total"><?= $sales_data['sale_total']!=''?$sales_data['sale_total']:'0:00';?></span>
                                <input type="hidden" name="invoice_total" value="<?= $sales_data['sale_total'];?>" id="invoice_total">
                            </div>
                        </div>

            </div>
            </div>
            <div class="pull-right">

                <div class="">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 margin-top btn-group">
                                <input type="submit" id="action_edit_sale" class="btn btn-success float-right" value="Update Details" data-loading-text="Editing...">
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>

</form>

<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select an item</h4>
            </div>
            <div class="modal-body">
                <?=$this->crm_model->popProductsList(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="selected">Add</button>
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    .cart-item {
        max-height: 160px;
        overflow-y: scroll;
    }
</style>



