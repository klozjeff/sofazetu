	<?php
	

include_once('functions.php');

// show PHP errors
ini_set('display_errors', 1);
	if(isset($_GET['id']) && isset($_GET['type'])){
		
		if($_GET['type']=='Quotation')
		{
			$pre='Quote_';
		}
		else if($_GET['type']=='Invoice')
		{
			$pre='Invoice_';
		}
		$row=getParticularInvoice($_GET['type'],$_GET['id']);
		$customer_id=$row['cid'];
		$customer_name = $row['name']; // customer name
		$customer_email = $row['email']; // customer email
		$customer_address_1 = $row['address_1']; // customer address
		$customer_address_2 = $row['address_2']; // customer address
		$customer_town = $row['town']; // customer town
		$customer_county = $row['county']; // customer county
		$customer_postcode = $row['postcode']; // customer postcode
		$customer_phone = $row['phone']; // customer phone number
		
		//shipping
		$customer_name_ship = $row['name_ship']; // customer name (shipping)
		$customer_address_1_ship = $row['address_1_ship']; // customer address (shipping)
		$customer_address_2_ship = $row['address_2_ship']; // customer address (shipping)
		$customer_town_ship = $row['town_ship']; // customer town (shipping)
		$customer_county_ship = $row['county_ship']; // customer county (shipping)
		$customer_postcode_ship = $row['postcode_ship']; // customer postcode (shipping)
		
		
		//Billing Center
		$center_id=$row['sid'];
		$center_name=$row['center_name'];
		$center_town=$row['center_town'];
		$center_county=$row['center_county'];

		// invoice details
		$invoice_number = $row['invoice']; // invoice number
		$custom_email = $row['custom_email']; // invoice custom email body
		$invoice_date = $row['invoice_date']; // invoice date
		$invoice_due_date = $row['invoice_due_date']; // invoice due date
		$invoice_subtotal = $row['subtotal']; // invoice sub-total
		$invoice_shipping = $row['shipping']; // invoice shipping amount
		$invoice_discount = $row['discount']; // invoice discount
		$invoice_vat = $row['vat']; // invoice vat
		$invoice_total = $row['total']; // invoice total
		$invoice_notes = $row['notes']; // Invoice notes
		$invoice_type = $row['invoice_type']; // Invoice type
		$invoice_status = $row['status']; // Invoice status
		$lpo_no = $row['lpo_no']; // Invoice status
		
		//People
		$person_name=$row['full_name'];
		$person_contact=$row['mphone'];
		//unlink('invoices/'.$_GET['id'].'.pdf');
	unlink('invoices/'.$pre.''.$_GET['id'].'.pdf');
		//Set default date timezone
		date_default_timezone_set(TIMEZONE);
		//Include Invoicr class
		include('invoice.php');
		//Create a new instance
		$invoice = new invoicr("A4",CURRENCY,"en");
		//Set number formatting
		$invoice->setNumberFormat('.',',');
		//Set your logo
		$invoice->setLogo(COMPANY_HEADER_LOGO,COMPANY_HEADER_LOGO_WIDTH,COMPANY_HEADER_LOGO_HEIGHT);
		//Set theme color
		$invoice->setColor(INVOICE_THEME);
		//Set type
		$invoice->setType($invoice_type);
		//Set reference
		$invoice->setReference($invoice_number);
		//Set LPO
		$invoice->setLPO($lpo_no);
		//Set date
		$invoice->setDate($invoice_date);
		//Set due date
		$invoice->setDue($invoice_due_date);
		//Set from
		$invoice->setFrom(array(COMPANY_NAME,COMPANY_ADDRESS_1,COMPANY_ADDRESS_2,COMPANY_COUNTY,COMPANY_POSTCODE,COMPANY_NUMBER,COMPANY_VAT));
		//Set to
		$invoice->setTo(array($customer_name,$customer_address_1,$customer_address_2,$customer_town,$customer_county,$customer_postcode,"Phone: ".$customer_phone,$person_name,$person_contact));
		//Ship to
		$invoice->shipTo(array($center_name,$customer_address_1_ship,$customer_address_2_ship,$center_town,$center_county,$customer_postcode_ship,''));
		//Add items
		// invoice product items
		
		// Connect to the database
		$db_prefix=DATABASE_PREFIX;
						$mysqli = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

						// output any connection error
						if ($mysqli->connect_error) {
							die('Error : ('.$mysqli->connect_errno .') '. $mysqli->connect_error);
						}

						// the query
					
					$query2 = "SELECT * FROM {$db_prefix}invoice_items p WHERE p.invoice_type='".$mysqli->real_escape_string($_GET['type'])."' AND p.invoice = '".$mysqli->real_escape_string($_GET['id']) . "'";
					$result2 = mysqli_query($mysqli, $query2);
						while ($rows = mysqli_fetch_assoc($result2)) {
							
							//	foreach($rows['product'] as $key => $value) 
							//	{
		    $item_product = $rows['product'];
		    $item_qty = $rows['qty'];
		    $item_price = $rows['price'];
		    $item_discount = $rows['discount'];
		    $item_subtotal = $rows['subtotal'];

		   	if(ENABLE_VAT == true) {
		   		$item_vat = (VAT_RATE / 100) * $item_subtotal;
		   	}

		    $invoice->addItem($item_product,'',$item_qty,$item_vat,$item_price,$item_discount,$item_subtotal);
		//}
						}
		//Add totals
		$invoice->addTotal("Sub-Total",$invoice_subtotal);
		if(!empty($invoice_discount)) {
			$invoice->addTotal("Discount",$invoice_discount);
		}
		if(!empty($invoice_shipping)) {
			$invoice->addTotal("Delivery",$invoice_shipping);
		}
		if(ENABLE_VAT == true) {
			$invoice->addTotal("TAX/VAT ".VAT_RATE."%",$invoice_vat);
		}
		$invoice->addTotal("Total",$invoice_total,true);
		//Add Badge
		$invoice->addBadge($invoice_status);
		// Customer notes:
		if(!empty($invoice_notes)) {
			$invoice->addTitle("Notes");
			$invoice->addParagraph($invoice_notes);
		}
		//Set Invoice Reference Notes
		$invoice->setInfo($invoice_notes);
		
		//Add Title
		$invoice->addTitle("Payment information");
		//Add Paragraph
		$invoice->addParagraph(PAYMENT_DETAILS);
		//Set footer note
	
			$invoice->setFooternote(FOOTER_NOTE_IMAGE,FOOTER_NOTE_IMAGE_WIDTH,FOOTER_NOTE_IMAGE_HEIGHT);
		$invoice->setFooternoteLink(FOOTER_NOTE);
		//Render the PDF
		$invoice->render('invoices/'.$pre.''.$invoice_number.'.pdf','F');
		header('Location:http://localhost/invoiceapp/app/invoices/'.$pre.''.$invoice_number.'.pdf');
	    
	}
	?>