<?php error_reporting(0);$array=explode(',',$sales_data['sale_accessories']); ?>
<!DOCTYPE html>
<html>
<head>

</head>
    <body >
    <div class="row" style="height:100% !important;border:1px solid #bce8f1;border-radius:4px;font-family:Raleway, sans-serif;" >
        <div class="col-md-12">
            <div class="panel-default">
                <div class="panel-heading text-right" style="border-bottom:0px solid #fff !important" align="center">
    <span style="margin-top:15% !important;font-size:24px;font-weight:800;color:#3097D1"> Purchase Order</span></div>
            <div class="panel-body">
                    <div class="row">
                        <div style="margin-left:10%;width:30% !important;align:center">

                            <img alt="" src="http://localhost/sofazetu/public/image/SofaZetu-Logo.png" style="height:100px;" />

                        </div>
                         
    <div class="clearfix" style="color:#3097D1;font-weight:400;margin-top:-10%;margin-left:60% ;width:70% !important;" >
                                           
                                            <div class="">
										
												
												
                                                    <strong>SOFAZETU LTD</strong><br>
													<strong>P.O BOX 60596-00200 NAIROBI</strong><br/>
                                                <strong>CELL:0729 547 704 /0702 828 402 <br> 0790 795 566</strong><br/>
                                                <strong>Email:sales@sofazetu.co.ke</strong><br/>
												 <strong>Website:www.sofazetu.co.ke</strong><br/>
												 
												<span style="margin-left:80%;font-weight:bold">No.<span style="color:red;font-weight:bold"><?php echo $sales_data['uniqueID'];?></span></span>
                                                
                                            </div>
                                        </div>
                        <hr style="color:#3097D1;font-weight:400;"></hr>
                     
                    </div>
					 </div>
					  <div class="panel-body">
                    <div class="row">
                        <div style="color:#000;font-weight:400;margin-left:2%;margin-top:-5%;width:50% !important;align:center">

                            <div class="">
										
												
												
                                                    <strong>Dated: </strong><?php echo date('Y-m-d',strtotime($sales_data['delivery_date']));?><br>
													<strong>Purchase Order #:</strong><span style="underline !important">XIA/SOF/<?php echo $sales_data['uniqueID'];?>/<?php echo date('Y');?></span><br/><br/>
                                               <strong>Ship To: </strong><br>
													<strong>Name:</strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['name'];?><br/>
                                               <strong>Address: </strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['address'];?><br>
													<strong>Phone #:</strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['phone'];?><br/>
                                             
                                            </div>

                        </div>
                         
    <div class="clearfix" style="color:#000;font-weight:400;margin-top:-15%;margin-left:60% ;width:70% !important;" >
                                           
                                            <div class="">
										
												
												
                                                    <strong>Delivery Date </strong> <?php echo $sales_data['delivery_date'];?><br>
													
												 
											
                                            </div>
                                        </div>
                     
                     
                    </div>
					 </div>
               

</div>
</div>



 <div class="col-md-12" style="margin-top:15%">
            <div class="panel-default">
<table class="table table-bordered table-striped" style="color:#000;font-weight:400;cellspaing:2px">
<thead>
<tr>
<th>
SI.No
</th>
<th  >
Item Description
</th>
<th>
Qty
</th>
<th>
Price
</th>
<th>
Sub-Total
</th>
</tr>
</thead>
<tbody>
<?php
$t=1;
        foreach($sales_items as $item):
            echo '<tr>
			 <td colspan="1">'.$t++.'</td>
                <td>'.$item['product_id'].'</td>
                <td>'.$item['quantity'].'</td>
				  <td>'.$item['price'].'</td>
                <td>'.$item['sub_total'].'</td>
            </tr>';
        endforeach;
        ?>
</tbody>
</table>
</div></div>


 <div class="col-md-12" style="color:#000;font-weight:400;margin-top:5%">
            <div class="panel-default">
	  <div class="panel-body">
                    <div class="row">
                        <div style="padding:5px !important;margin-left:2%;margin-top:-5%;width:50% !important;align:center">
 <span style="font-size:12px;font-weight:bold">Accessories & Additional Information</span><br/>
                            <div class="">


                                    <?php $accessories=array();
                                    foreach ($sales_acc as $items):
                                        $accessories[]=$items;

                                    endforeach;
                                    $key = array_search('scatters', $accessories[0]);
                                    function searchKey($products, $field, $value)
                                    {
                                        foreach($products as $key => $product)
                                        {
                                            if ( $product[$field] === $value )
                                                return $key;
                                        }
                                        return false;
                                    }
                                    if(searchKey($accessories,'product_id','comforters')!==false):
                                        $com=searchKey($accessories,'product_id','comforters');
                                        $avcomm=1;
                                    else:
                                        $avcomm=0;
                                    endif;
                                    if(searchKey($accessories,'product_id','scatters')!==false):
                                        $sca=searchKey($accessories,'product_id','scatters');
                                        $avsca=1;
                                    else:
                                        $avsca=0;
                                    endif;
                                    if(searchKey($accessories,'product_id','footrest')!==false):
                                        $foo=searchKey($accessories,'product_id','footrest');
                                        $avfoo=1;
                                    else:
                                        $avfoo=0;
                                    endif;
                                    if(searchKey($accessories,'product_id','others')!==false):
                                        $oth=searchKey($accessories,'product_id','others');
                                        $avoth=1;
                                    else:
                                        $avoth=0;
                                    endif;
                                    ?>
<br/>

<table class="table table-bordered table-striped" style="color:#000;font-weight:100;cellspaing:2px">
<thead style="font-size:10px !important;">
<tr>
<th>
SI.No
</th>
<th  >
Item Description
</th>
<th>
Qty
</th>
<th>
Price
</th>
<th>
Sub-Total
</th>
</tr>
</thead>
<tbody>
<?php
$t=1;
        foreach($array as $itemss):
		if(!empty($itemss)):
		$this->db->select('*');
    $this->db->from('sale_items');
    $this->db->where("product_id", $itemss);
    $query = $this->db->get();
            echo '<tr>
			 <td colspan="1">'.$t++.'</td>
                <td>'.$itemss.'</td>
                <td>'.$query->row()->quantity.'</td>
				  <td>'.$query->row()->price.'</td>
                <td>'.$query->row()->sub_total.'</td>
            </tr>';
			endif;
        endforeach;
        ?>
</tbody>
</table>


                              <!--  <strong>Dated: </strong><?php echo date('Y-m-d',strtotime($sales_data['delivery_date']));?><br>
													<strong>Purchase Order #:</strong><span style="underline !important">XIA/SOF/<?php echo $sales_data['uniqueID'];?>/<?php echo date('Y');?></span><br/><br/>
                                               <strong>Ship To: </strong><br>
													<strong>Name:</strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['name'];?><br/>
                                               <strong>Address: </strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['address'];?><br>
													<strong>Phone #:</strong><?php echo $this->crm_model->getDataById('customers',$sales_data['customer_id'])['phone'];?><br/>
                                             -->
                                            </div>

                        </div>
                         
    <div class="clearfix" style="margin-top:-15.5%;margin-left:60% ;width:70% !important;" >
                                           <span style="text-transform:uppercase;font-size:12px;font-weight:bold">Payment Terms:</span>
                                            <div class="">
										
												
												
                                                    <strong>	Deposit: </strong> <?php echo $sales_data['sale_deposit'];?><br>
													
												 <strong>	Balance: </strong> <?php echo $sales_data['sale_balance'];?><br>
											<strong>	Sub-Total: </strong> <?php echo $sales_data['sale_subtotal'];?><br>
											<strong>	Additional Accessories: </strong> <?php echo $sales_data['sale_accessories_total'];?><br>
											<strong>	Delivery Fee: </strong> <?php echo $sales_data['sale_transport'];?><br>
											<strong>	Total Amount: </strong> <?php echo $sales_data['sale_total'];?><br>
                                            </div>
                                        </div>
                     
                     
                    </div>
					 </div>
</div></div>
<div class="col-md-12" style="margin-top:40%">
                                      
                                       <div class="" role="alert" align="center">
  <b>Note:</b> This Purchase Order is computer generated and therefore not signed. It is valid document issued under the authority of
SofaZetu Limited
<br/><br/><span style="font-size:8px !important;color:orange">Order once placed can't be cancelled.Any such action will attract a percentage penalty fee on Order amount</span>
</div>
	</div>												

</div>


<body>
</html>

            