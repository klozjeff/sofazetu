<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 6:35 PM
 */
?><!--
<div class="col-md-12">
    <div class="panel panel-default">

        <div class="panel-body">
            <?php
            echo form_open(base_url() . 'index.php/crm/sales', array(
                'method' => 'get',
                'id' => '',
                'class'=>'form'
            ));
            ?>
            <div class="form-group">
                <label for="price">Date Range</label>
                <select class="form-control" id="date-range" name="date_range">
                    <option value="">-- Select Date Range --</option>
                    <option value="today">Today</option>
                    <option value="current_week">This Week</option>
                    <option value="current_month">This Month</option>
                </select>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
            </form>
        </div>
    </div>
</div>-->
<div class="col-md-12">
    <?php
    echo form_open(base_url() . 'crm/sales', array(
        'method' => 'get',
        'id' => '',
        'class'=>'form'
    ));
    ?>
    <div class="panel panel-default">

        <div class="panel-body">
            <div id="response" class="alert alert-success" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <div class="message"></div>
            </div>
            <div class="form-group">
                <div class="col-md-1">
<label class="" style="margin-top: 10px !important;">Filter</label>

                </div>
                <div class="col-md-2">


                    <div class="input-group date" id="s_date">

                        <input type="text" class="form-control required" name="s_date" placeholder="Start Date" data-date-format="YYYY-MM-DD" />
                        <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
                    </div>
                </div>
                <div class="col-md-2">


                    <div class="input-group date" id="e_date">
                        <input type="text" class="form-control required" name="e_date" placeholder="End Date" data-date-format="YYYY-MM-DD" />
                        <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
                    </div>
                </div>
                <div class="col-md-2">


                    <select name="sale_type" id="sale_type" class="form-control required">
                        <option value="">Select Category</option>
                        <option value="Order">Order</option>
                        <option value="Ready">Ready Made</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="sale_status" id="sale_status" class="form-control required">
                        <option value="">Select Status</option>
                        <option value="In Progress">In Progress</option>
                        <option value="Completed">Completed</option>
                        <option value="Suspended">Suspended</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>
                <div class="col-md-2">


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>


            </div>


        </div>
    </div>
</form>
    <div class="panel panel-default">
        <div class="panel-heading">Sales</div>
        <div class="panel-body">
        <table id="data-table" class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Cashier</th>
                <?php if($this->session->userdata('Role')==1) { ?>
                <th>Branch/Location</th>
                <?php } ?>
                <th>Customer</th>
                <th>Sale Type</th>
                <th>D.Date</th>
                <th>Sub-Total</th>
                <th>D.Fee</th>
                <th>Total</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($sales)):
                $t=1;
            foreach ($sales as $sale):
               echo '<tr>
                <td>'.$t++.'</td>
                <td>'.$this->crm_model->getDataById('users',$sale['cashier_id'])['name'].'</td>';
               if($this->session->userdata('Role')==1) {
                echo '<td>'.$this->crm_model->getDataById('branch',$sale['sale_branch_id'])['name'].'</td>';
             }
             echo '<td>'.$this->crm_model->getDataById('customers',$sale['customer_id'])['name'].'</td>
               <td>'.$sale['sale_type'].'</td>
                <td>'.$sale['delivery_date'].'</td>
                    <td>'.$sale['sale_subtotal'].'</td>
           <td>'.$sale['sale_transport'].'</td>
            <td>'.$sale['sale_total'].'</td>
                <td>'.date('Y-m-d',strtotime($sale['created_at'])).'</td>
                 <td>'.$sale['status'].'</td>';
                if($sale['status']=='Completed' || $sale['status']=='Suspended' || $sale['status']=='Cancelled'){
                echo '<td>
					<a href="'.base_url().'crm/sales/invoice/'.$sale['id'].'" class="btn btn-danger btn-xs pull-right btn-delete" target="_blank">Doc</a>
                    <a href="" class="btn btn-default btn-xs pull-right">View/Edit</a>
                </td>';
                }
                else
                {
                    echo '<td>
					<a href="'.base_url().'crm/sales/invoice/'.$sale['id'].'" class="btn btn-danger btn-xs pull-right btn-delete" target="_blank"><i class="fa fa-print"></i>Doc</a>
                    <a href="'.base_url().'crm/sales/view/'.$sale['id'].'" class="btn btn-primary btn-xs pull-right">View/Edit</a>
                </td>';
                }

            echo '</tr>';

            endforeach;
            endif;
            ?>
            </tbody>
        </table>
    </div>
    </div>
</div>

