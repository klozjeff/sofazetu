<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 6:35 PM
 */
?>

<form id="create_sale" action="">
        <div class="col-md-8">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div id="response" class="alert alert-success" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <div class="message"></div>
                    </div>
                    <div class="form-group">
<div class="col-md-3">


    <div class="input-group " id="invo">
        <span class="input-group-addon">XIA/SF/</span>
        <input type="text" name="sale_id" id="sale_id" class="form-control required" readonly placeholder="Sale Number"  aria-describedby="sizing-addon1" value="<?= $this->crm_model->getLastSaleID(); ?>">

    </div>
</div>

                        <div class="col-md-3">


                            <select name="sale_type" id="sale_type" class="form-control required">
                                <option value="">Select Category</option>
                                <option value="Order">Order</option>
                                <option value="Ready Made">Ready Made</option>
                                <option value="New Sale">New Sale</option>
                            </select>
                        </div>

                        <div class="col-md-3">


                            <div class="input-group date" id="sale_delivery_date">
                                <input type="text" class="form-control required delivery_date" name="delivery_date" placeholder="Delivery Date" value="<?= date('Y-m-d', strtotime(' + 10 days')); ?>" data-date-format="YYYY-MM-DD" />
                                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
                            </div>
                        </div>
<?php if($this->session->userdata('Role')==1){?>
                        <div class="col-md-3">
                                <select class="form-control" id="branch_id" name="branch_id">
                                    <option value="">Select Location</option>
                                    <?php $branchDetails=$this->db->get('branch')->result_array();
                                    foreach ($branchDetails as $branchDetail):
                                        echo  '<option value="'.$branchDetail['id'].'">'.$branchDetail['name'].'</option>';
                                    endforeach;?>



                            </select>
                        </div>
                        <?php }

                        else {
    ?>

                            <div class="col-md-3">
                                <input type="hidden" value="<?php echo $this->session->userdata('Branch');?>" class="form-control" id="branch_id" name="branch_id">

                            </div>
                       <?php }?>

                    </div>


                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>
                <div class="panel-body">
                    <div class="row">
                        <!--
                        <div class="col-md-2">
                            <div class="list-group">
                                <a href="#" v-for="category in categories" class="list-group-item list-group-item-info" v-on:click="setSearchItemKey(category.id)" track-by="$index" >{{ category.name }}</a>
                            </div>
                        </div>
                        -->
                        <div class="col-md-12" style="padding: 25px !important;">
                          <!--  <input type="text" class="form-control" placeholder="Search for product" v-model="searchProductQuery.q" onkeyup="searchProduct">
                            <br />-->
                            <div class="row">
                                <table id="invoice_table" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            <a href="#" class="add-row"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                        </th>
                                        <th width="350px">
                                          Product or Service
                                        </th>
                                        <th>
                                          Qty/Set
                                        </th>
                                        <th>
                                         Price
                                        </th>

                                        <th>
                                         Sub Total
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> <a href="#" class="delete-row"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>

                                        </td>
                                        <td>
                                            <div class="form-group form-group-sm  no-margin-bottom">
                                                <textarea type="text" rows='8' class="form-control form-group-sm item-input invoice_product required" name="pos_product[]" placeholder="Enter product Name and / or description"></textarea>
                                                <p class="item-select">or <a href="#">Select a product</a></p>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="form-group form-group-sm no-margin-bottom">
                                                <input type="text" class="form-control invoice_product_qty calculate" name="pos_product_qty[]" value="1">
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="input-group input-group-sm  no-margin-bottom">
                                                <span class="input-group-addon"></span>
                                                <input type="text" class="form-control calculate invoice_product_price required" name="pos_product_price[]" aria-describedby="sizing-addon1" placeholder="0.00">
                                            </div>
                                        </td>

                                        <td class="text-right">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon"></span>
                                                <input type="text" class="form-control calculate-sub" name="pos_product_sub[]" id="invoice_product_sub" value="0.00" aria-describedby="sizing-addon1" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" id="accessories">

                    <div class="row"> <div class="col-xs-5">&nbsp;<span class="">Accessories & Additional Information<br/></span></div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-1">
                            Qty
                        </div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-1">
                           Price
                        </div>
                        <div class="col-xs-2">
                          &nbsp; &nbsp; Sub Total
                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            1. Comforters
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" name="box[]" value="comforters" id="box1" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text"  disabled="disabled" id="access_qty_1" class="form-control access_qty_1 " name="pos_qty_comforters" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_price_1" class="form-control calculateaccess access_price_1" name="pos_price_comforters" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" class="form-control calculateaccess invoice_access_price1" name="pos_subtotal_comforters" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            2. Sausage Scatters
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" id="box2" name="box[]"  value="scatters" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_qty_2" class="form-control access_qty_2 " name="pos_qty_scatters" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_price_2" class="form-control calculateaccess access_price_2 " name="pos_price_scatters" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="invoice_access_price2" class="form-control calculateaccess invoice_access_price2" name="pos_subtotal_scatters" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            3. FootRest
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" id="box3" name="box[]" value="footrest" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_qty_3" class="form-control access_qty_3 " name="pos_qty_footrest" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_price_3" class="form-control calculateaccess access_price_3 " name="pos_price_footrest" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="invoice_access_price3" class="form-control calculateaccess invoice_access_price3" name="pos_subtotal_footrest" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-left: 5%">
                        <div class="col-xs-3">
                            4. Others
                        </div>
                        <div class="col-xs-2">
                            <input type="checkbox" id="box4" name="box[]" value="others" class="remove_vat">
                        </div>
                        <div class="">
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_qty_4" class="form-control access_qty_4 " name="pos_qty_others" placeholder="1">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="access_price_4" class="form-control calculateaccess access_price_4 " name="pos_price_others" placeholder="0.00">
                            </div>
                            <div class="col-xs-2 form-group form-group-sm no-margin-bottom">
                                <input type="text" disabled="disabled" id="invoice_access_price4" class="form-control calculateaccess invoice_access_price4" name="pos_subtotal_others" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>

                        </div>
                    </div>

                </div>


            </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>
                <div class="panel-body">
                    <div class="form-group">

                            <label class="control-label">Customer Name</label>

                            <input type="text" class="form-control required" name="customer_name">

                            <label class="control-label">Contacts</label>

                            <input type="text" class="form-control required" name="phone">
                        <label class="control-label">Address</label>

                        <textarea type="text" rows="1" class="form-control required" name="address"></textarea>

                    </div>


                </div>
            </div>
            <div id="pay" class="panel panel-default">
                <div class="panel-heading">Payment</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <strong class="shipping">Deposit:</strong>
                        </div>
                        <div class="col-xs-6">
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Kshs</span>
                                <input type="text" class="form-control deposit" id="pos_deposit" name="pos_deposit" aria-describedby="sizing-addon1" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>Bal:</strong>
                        </div>
                        <div class="col-xs-6">
                            <span class="pos-balance">0.00</span>
                            <input type="hidden" name="pos_balance" id="pos_balance">
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Sub Total:</strong>
                            </div>
                            <div class="col-xs-6">
                               <span class="invoice-sub-total">0.00</span>
                                <input type="hidden" name="invoice_subtotal" id="invoice_subtotal">
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>Add. Accessories:</strong>
                        </div>
                        <div class="col-xs-6">
                            <span class="invoice-additional">0.00</span>
                            <input type="hidden" name="invoice_additional" id="invoice_additional">
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <strong class="shipping">Delivery Cost:</strong>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">Kshs</span>
                                    <input type="text" class="form-control calculate shipping" name="invoice_shipping" aria-describedby="sizing-addon1" placeholder="0.00">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Total:</strong>
                            </div>
                            <div class="col-xs-6">
                                <span class="invoice-total">0.00</span>
                                <input type="hidden" name="invoice_total" id="invoice_total">
                            </div>
                        </div>

            </div>
            </div>
            <div class="pull-right">

                <div class="">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 margin-top btn-group">
                                <input type="submit" id="action_create_sale" class="btn btn-success float-right" value="Save Details" data-loading-text="Creating...">
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>

</form>

<div id="insert" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select an item</h4>
            </div>
            <div class="modal-body">
                <?=$this->crm_model->popProductsList(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="selected">Add</button>
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    .cart-item {
        max-height: 160px;
        overflow-y: scroll;
    }
</style>



