<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 6:19 PM
 */
?>
<link href="<?php echo base_url();?>public/css/main.css" rel="stylesheet">
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
            <div class="all-wrapper">
                <div class="layout-w">
                    <div class="content-w">


                        <div class="content-i">
                            <div class="content-box">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="element-wrapper">
                                            <div class="element-actions">

                                                    <?php
                                                    echo form_open(base_url() . 'index.php/crm/dashboard', array(
                                                        'method' => 'get',
                                                        'id' => '',
                                                        'class'=>'form-inline justify-content-sm-end'
                                                    ));
                                                    ?><select name="filter" class="form-control form-control-sm rounded">
                                                        <option value="today">Today</option>
                                                        <option value="week">This Week</option>
                                                        <option value="month">This Month</option>
                                                    </select> <button type="submit" value="submit" name="submit"><i class="os-icon os-icon-pencil-1"></i></button></form>
                                            </div>
                                            <h6 class="element-header">Sales Dashboard</h6>
                                            <div class="element-content">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">Total Orders</div>
                                                            <div class="value"><?php echo $sales_no;?></div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">Total Sales</div>
                                                            <div class="value">Kshs<?php echo number_format($total_sales[0]['sale_total'],0);?></div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">New Customers</div>
                                                            <div class="value"><?php echo $total_customers;?></div>
                                                           <!-- <div class="trending trending-down-basic"><span>9%</span><i
                                                                        class="os-icon os-icon-graph-down"></i></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="element-wrapper"><h6 class="element-header">New Orders</h6>
                                            <div class="element-box">
                                                <div class="table-responsive">
                                                    <table class="table table-lightborder">
                                                        <thead>
                                                        <tr>
                                                            <th>Sale ID</th>
                                                            <th>Customer</th>
                                                            <th class="text-center">Status</th>
                                                            <th class="text-right">Order Total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($latest_sales as $newsale):
                                                            if($newsale['status']=='In Progress'):
                                                                $class='yellow';
                                                            elseif($newsale['status']=='Completed'):
                                                                    $class='green';
                                                            elseif($newsale['status']=='Cancelled' || $newsale['status']=='Suspended' ):
                                                                $class='red';
                                                            endif;

                                                        echo '<tr>
                                                            <td>#'.$newsale['uniqueID'].'</td>
                                                            <td class="nowrap">'.$this->crm_model->getDataById('customers',$newsale['customer_id'])['name'].'</td>
                                                    
                                                            <td class="text-center">
                                                            
                                                                <div class="status-pill '.$class.'" title="'.$newsale['status'].'"
                                                                     data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">Kshs '.$newsale['sale_total'].'</td>
                                                        </tr>';
                                                        endforeach;
                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="element-wrapper"><h6 class="element-header">Orders in Nos</h6>
                                            <div class="element-box">
                                                <div class="el-chart-w">
                                                    <iframe class="chartjs-hidden-iframe"
                                                            style="display: block; overflow: hidden; border: 0px none; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"
                                                            tabindex="-1"></iframe>
                                                    <canvas height="138" id="donutChart" width="138"
                                                            style="display: block; width: 128px; height: 20px;"></canvas>
                                                    <div class="inside-donut-chart-label"><strong><?= $this->crm_model->getSalesTotalByType(); ?></strong><br/><span style="fon-size:12px !important;">Total Orders</span>
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="el-legend">
                                                    <div class="legend-value-w">
                                                        <div class="legend-pin"
                                                             style="background-color: #71c21a;"></div>
                                                        <div class="legend-value">Completed(<?= $this->crm_model->getSalesTotalByType('Completed'); ?>)</div>
                                                    </div>
                                                    <div class="legend-value-w">
                                                        <div class="legend-pin"
                                                             style="background-color: #f8bc34;"></div>
                                                        <div class="legend-value">In Progress(<?= $this->crm_model->getSalesTotalByType('In Progress'); ?>)</div>
                                                    </div>
                                                    <div class="legend-value-w">
                                                        <div class="legend-pin"
                                                             style="background-color: #d97b70;"></div>
                                                        <div class="legend-value">Suspended(<?= $this->crm_model->getSalesTotalByType('Suspended'); ?>)</div>
                                                    </div>
                                                    <div class="legend-value-w">
                                                        <div class="legend-pin"
                                                             style="background-color: #d97b70;"></div>
                                                        <div class="legend-value">Cancelled(<?= $this->crm_model->getSalesTotalByType('Cancelled'); ?>)</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="content-panel">
                                <div class="element-wrapper"><h6 class="element-header">Quick Links</h6>
                                    <div class="element-box-tp">
                                        <div class="el-buttons-list full-width"><a class="btn btn-white btn-sm" href="<?php base_url();?>sales/create"><i class="os-icon os-icon-delivery-box-2"></i><span> Create New Sale</span></a><a
                                                    class="btn btn-white btn-sm" href="<?php base_url();?>products/create"><i class="os-icon os-icon-window-content"></i><span> Add New Product</span></a><a
                                                    class="btn btn-white btn-sm" href="<?php base_url();?>sales"><i class="os-icon os-icon-wallet-loaded"></i><span> View Daily Sales</span></a></div>
                                    </div>
                                </div>
                                <div class="element-wrapper"><h6 class="element-header">System Users</h6>
                                    <div class="element-box-tp">
<?php
if(!empty($users)):
    $t=1;
    foreach ($users as $user):?>

                                        <div class="profile-tile">
                                            <div class="profile-tile-box">
                                                <div class="pt-avatar-w"><img alt="" src="<?php echo base_url();?>public/image/User-48.png"></div>
                                                <div class="pt-user-name"><?php echo $user['name'];?></div>
                                            </div>
                                            <span class="profile-tile-meta">
                                                <ul>
                                                    <li>Last Login:<?php if($user['logged_in']==1):?> Online<?php else: echo $user['last_login']; endif;?></li>
                                                    <li>Sales/Orders Processed:<strong><?= $this->crm_model->getSalesByUser($user['id']);?></strong></li>

                                                </ul>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        endforeach;
                                        endif;
                                        ?>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>
</div>


