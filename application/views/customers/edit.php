
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Customers - Edit</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/customers/edit', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>
                        <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="customerID" value="<?php echo $customer['id'];?>">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name"  value="<?php echo $customer['name'];?>">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $customer['email'];?>">
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $customer['phone'];?>">
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address"><?php echo $customer['address'];?></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-link" href="">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
