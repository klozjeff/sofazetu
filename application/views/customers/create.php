
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Customers - Create</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/customers/create', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>
                      
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="">
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="">
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link" href="<?php echo base_url().'crm/customers';?>">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
  