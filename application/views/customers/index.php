
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Customers
                    <div class="pull-right">
                        <a href="<?php echo base_url().'crm/customers/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
                <div class="panel-body">
                <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
        if(!empty($customers)):
            $t=1;
                    foreach($customers as $customer):
                        echo '<tr>
                            <td>'.$t++.'</td>
                            <td>'.$customer['name'].'</td>
                            <td>'.$customer['email'].'</td>
                            <td>'.$customer['phone'].'</td>
                            <td>';
                        echo form_open(base_url() . 'crm/common/delete', array(
                            'method' => 'post',
                            'id' => '',
                            'class'=>'form-inline'
                        ));

                              echo'
                                  <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="action" value="customers">
                                     <input type="hidden" name="db" value="customers">
                                   <input type="hidden" name="rowID" value="'.$customer['id'].'">
                                    <button class="btn btn-danger btn-xs pull-right btn-delete" name="delete" type="submit">Delete</button>
                                </form>
                                <a href="'.base_url().'crm/customers/edit/'.$customer['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
                    endforeach;
        else:
           echo '<tr>
    <td colspan="" align="center">No data Available</td>
</tr>';
        endif;
        ?>



                </table>
                </div>

            </div>
        </div>
