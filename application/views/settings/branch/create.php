<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">Settings Menu</div>
        <ul class="list-group">
            <a href="<?php echo base_url().'crm/settings/profile';?>"  class="list-group-item">Profile</a>
            <?php if($this->crm_model->check_permissions('role','index')):?>
            <a href="<?php echo base_url().'crm/settings/roles';?>"  class="list-group-item">Roles</a>
            <?php endif; if ($this->crm_model->check_permissions('branch','index')):?>
			<a href="<?php echo base_url().'crm/settings/branch';?>"  class="list-group-item">Locations/Branches</a>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Branch/location - Create</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/settings/branch/create', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="">
                        </div>
						
						
                       

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link" href="">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

