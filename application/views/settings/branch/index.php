
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Branches/Location
                    <div class="pull-right">

                        <a href="<?php echo base_url().'crm/settings/branch/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
                <div class="panel-body">


                <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
        if (!empty($branch)):
            $t=1;
                    foreach ($branch as $single_branch):
                        echo'
                        <tr>
                            <td>'.$t++.'</td>
                            <td>'.$single_branch['name'].'</td>
                            <td>
                                <a href="'.base_url().'crm/settings/branch/edit/'.$single_branch['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
                    endforeach;
        else:
            echo '<tr>
    <td colspan="3" align="center">No data Available</td>
</tr>';
        endif;
        ?>

                    </tbody>
                </table>

                </div>
            </div>
        </div>
