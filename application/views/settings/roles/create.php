<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">Settings Menu</div>
        <ul class="list-group">
            <a href="<?php echo base_url().'crm/settings/profile';?>"  class="list-group-item">Profile</a>
            <?php if($this->crm_model->check_permissions('role','index')):?>
                <a href="<?php echo base_url().'crm/settings/roles';?>"  class="list-group-item">Roles</a>
            <?php endif; if ($this->crm_model->check_permissions('branch','index')):?>
                <a href="<?php echo base_url().'crm/settings/branch';?>"  class="list-group-item">Locations/Branches</a>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading">Roles - Create</div>

    <div class="panel-body">
       <?php
                    echo form_open(base_url() . 'crm/settings/roles/create', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"></textarea>
            </div>

            <div class="form-group">
                <label for="permission_ids">Permissions</label>
                <div class="row">
				<?php 
	$this->db->select('object');
    $this->db->from('permissions');
    $this->db->group_by("object");
    $permissions = $this->db->get()->result_array();
                foreach($permissions as $object):
                    ?>
					<div class="col-md-3">
                        <h5><strong><?php echo $object['object'];?></strong></h5>
                       <?php 
					   $this->db->select('*');
    $this->db->from('permissions');
    $this->db->where("object",$object['object']);
    $controller = $this->db->get()->result_array();
	foreach($controller as $permission):?>
                        <div class="checkbox">
                            <label><input type="checkbox" value="<?php echo $permission['id'];?>" name="permission_ids[]"><?php echo $permission['action'];?></label>
                        </div>
                        <?php endforeach;?>
                    </div>
					<?php 
                endforeach;
				?>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-link" href="">Cancel</a>
            </div>
        </form>
    </div>
</div>
</div>