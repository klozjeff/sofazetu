<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">Settings Menu</div>
        <ul class="list-group">
            <a href="<?php echo base_url().'crm/settings/profile';?>"  class="list-group-item">Profile</a>
            <?php if($this->crm_model->check_permissions('role','index')):?>
                <a href="<?php echo base_url().'crm/settings/roles';?>"  class="list-group-item">Roles</a>
            <?php endif; if ($this->crm_model->check_permissions('branch','index')):?>
                <a href="<?php echo base_url().'crm/settings/branch';?>"  class="list-group-item">Locations/Branches</a>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading">Roles
        <div class="pull-right">
            <a href="<?php echo base_url().'crm/settings/roles/create';?>" class="btn btn-primary btn-xs">Create</a>
        </div>
    </div>
<div class="panel-body">
    <table id="data-table" class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
            
                <th></th>
            </tr>
        </thead>
        <tbody>
		<?php if(!empty($roles)):
		$t=1;
        foreach ($roles as $role):
            echo '<tr>
                <td>'.$t++.'</td>
                <td>'.$role['name'].'</td>
                <td>'.$role['description'].'</td>
                <td>
                    <a href="'.base_url().'crm/settings/roles/edit/'.$role['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                </td>
            </tr>';
       endforeach;
	  else:
           echo '<tr>
    <td colspan="4" align="center">No data Available</td>
</tr>';
        endif;
        ?>

        </tbody>
    </table>
	</div>

</div>
</div>