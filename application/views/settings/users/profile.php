<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">Settings Menu</div>
        <ul class="list-group">
            <a href="<?php echo base_url().'crm/settings/profile';?>"  class="list-group-item">Profile</a>
            <?php if($this->crm_model->check_permissions('role','index')):?>
                <a href="<?php echo base_url().'crm/settings/roles';?>"  class="list-group-item">Roles</a>
            <?php endif; if ($this->crm_model->check_permissions('branch','index')):?>
                <a href="<?php echo base_url().'crm/settings/branch';?>"  class="list-group-item">Locations/Branches</a>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading">User Profile</div>
    <div class="panel-body">
        <?php
        echo form_open(base_url() . 'crm/settings/profile', array(
            'method' => 'post',
            'id' => '',
            'class'=>'form'
        ));
        ?>
            <input type="hidden" name="_method" value="put">
        <input type="hidden" name="userID" value="<?= $userData['id'];?>">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="<?= $userData['name'];?>">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="<?= $userData['email'];?>">
            </div>

            <div class="form-group">
                <label for="role_id">Role</label>
                <input type="text" class="form-control" disabled id="role_name" name="role_name" value="<?=  $this->crm_model->getDataById('roles',$userData['role_id'])['name'];?>">
                <input type="hidden" name="role_id" value="<?= $userData['role_id'];?>">
            </div>
        <div class="form-group">
            <label for="email">Password <span style="color:red;font-size: 10px">**By Input new password you choose to reset old one</span></label>
            <input type="text" class="form-control" id="password" name="password" value="">
        </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
                <a class="btn btn-link" href="<?php echo base_url().'crm/settings/profile';?>">Cancel</a>
            </div>
        </form>
    </div>
</div>
</div>
