
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Users - Create</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/users/create', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>
                     
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="">
                        </div>

                        <div class="form-group">
                            <label for="role_id">Role</label>
                            <select class="form-control" id="role_id" name="role_id">
							 <option value="">Select Option</option>
							<?php $roleDetails=$this->db->get('roles')->result_array();
            foreach ($roleDetails as $roleDetail):
              echo  '<option value="'.$roleDetail['id'].'">'.$roleDetail['name'].'</option>';
            endforeach;?>
                               
                               
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="role_id">Branch</label>
                            <select class="form-control" id="branch_id" name="branch_id" required>
							 <option value="">Select Option</option>
							<?php $branchDetails=$this->db->get('branch')->result_array();
            foreach ($branchDetails as $branchDetail):
              echo  '<option value="'.$branchDetail['id'].'">'.$branchDetail['name'].'</option>';
            endforeach;?>
                               
                               
                            </select>
                        </div>
                        <hr>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>

                      

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link" href="<?php echo base_url().'crm/users';?>">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 