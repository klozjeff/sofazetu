
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users
                    <div class="pull-right">
                        <a href="<?php echo base_url().'crm/users/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
<div class="panel-body">
                <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(!empty($users)):
                    $t=1;
                    foreach ($users as $user):

                        echo '<tr>
                            <td>'.$t++.'</td>
                            <td>'.$user['name'].'</td>
                            <td>'.$user['email'].'</td>
                            <td>
                                <a href="'.base_url().'crm/users/edit/'.$user['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
                    endforeach;
                    else:
                        echo '<tr>
    <td colspan="4" align="center">No data Available</td>
</tr>';
                    endif;

                   ?>
                    </tbody>
                </table>
</div>

            </div>
        </div>
