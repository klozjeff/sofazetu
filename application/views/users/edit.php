
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Users - Edit</div>

                <div class="panel-body">
                    <form action="" method="POST">
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="userID" value="<?php echo $user['id'];?>">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $user['name'];?>">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $user['email'];?>">
                        </div>

                        <div class="form-group">
                            <label for="role_id">Role</label>
                            <select class="form-control" id="role_id" name="role_id">
                                    <?php $roleDetails=$this->db->get('roles')->result_array();
                                    foreach ($roleDetails as $roleDetail):
                                        ?>
                                        <option <?php if($user['role_id']==$roleDetail['id']) { echo "selected"; } ?> value="<?php echo $roleDetail['id'];?>"><?php echo $roleDetail['name'];?></option>
                                        <?php
                                    endforeach;?>

                                </select>
                        </div>
						
                        <div class="form-group">
                            <label for="role_id">Branch/Location</label>
                            <select class="form-control" id="branch_id" name="branch_id">
							<option value=""></option>
                                    <?php $branchDetails=$this->db->get('branch')->result_array();
                                    foreach ($branchDetails as $branchDetail):
                                        ?>
                                        <option <?php if($user['branch']==$branchDetail['id']) { echo "selected"; } ?> value="<?php echo $branchDetail['id'];?>"><?php echo $branchDetail['name'];?></option>
                                        <?php
                                    endforeach;?>

                                </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-link" href="{{ url('users') }}">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
