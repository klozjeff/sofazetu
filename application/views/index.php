<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 4:04 PM
 */
include 'partials/header.php';
?>
    <body>
    <div id="app">
        <?php
        include 'partials/navbar.php';
        //   include 'partials/notification.php';
?>

        <div class="container">
            <div class="row">
                <?php
       include $page_name.'.php';
?>
            </div>
        </div>

            </div>
    <!-- Scripts -->



    <script src="<?php echo base_url();?>public/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url();?>public/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/js/moment.js"></script>
    <script src="<?php echo base_url();?>public/js/bootstrap.datetime.js"></script>
    <script src="<?php echo base_url();?>public/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>public/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>public/js/pos.js"></script>


    <script src="<?php echo base_url();?>public/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>public/js/pdfmake.min.js"></script>
     <script src="<?php echo base_url();?>public/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>public/js/buttons.html5.min.js"></script>

    <script src="<?php echo base_url();?>public/js/Chart.min.js"></script>

</body>
</html>