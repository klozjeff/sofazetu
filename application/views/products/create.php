
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Products - Create</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/products/create', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>


                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="">
                        </div>
						
						
                        <div class="form-group">
                            <label for="price">Min. Price</label>
                            <input type="text" class="form-control" id="min_price" name="min_price" value="">
                        </div>

                        <div class="form-group">
                            <label for="price">Recommended Price</label>
                            <input type="text" class="form-control" id="price" name="price" value="">
                        </div>
						
						<div class="form-group">
                            <label for="price">Category</label>
                            <select type="text" class="form-control" id="category" name="category" value="">
							<option value="">Select Option</option>
								<?php $categoryDetails=$this->db->get('categories')->result_array();
            foreach ($categoryDetails as $categoryDetail):
              echo  '<option value="'.$categoryDetail['id'].'">'.$categoryDetail['name'].'</option>';
            endforeach;?>
							
							</select>
                        </div>
                    <div class="form-group">
                        <label for="price">Specs</label>
                        <textarea type="text" class="form-control" id="specs" name="specs" ></textarea>
                    </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link" href="<?php echo base_url().'crm/products';?>">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
