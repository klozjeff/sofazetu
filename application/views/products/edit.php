
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Products - Edit</div>

                <div class="panel-body">
                    <?php
                    echo form_open(base_url() . 'crm/products/edit', array(
                        'method' => 'post',
                        'id' => '',
                        'class'=>'form'
                    ));
                    ?>
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="productID" value="<?php echo $product['id'];?>">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $product['name'];?>">
                        </div>

                        <div class="form-group">
                            <label for="price">Min Price</label>
                            <input type="text" class="form-control" id="price" name="min_price" value="<?php echo $product['min_price'];?>">
                        </div>
                        <div class="form-group">
                            <label for="price">Recommended Price</label>
                            <input type="text" class="form-control" id="price" name="price" value="<?php echo $product['price'];?>">
                        </div>
                        <div class="form-group">
                            <label for="price">Category</label>
                            <select type="text" class="form-control" id="category" name="category" value="">
                                <option value="">Select Option</option>
                                <?php $categoryDetails=$this->db->get('categories')->result_array();
                                foreach ($categoryDetails as $categoryDetail):
                                    ?>
                                    <option <?php if($product['category']==$categoryDetail['id']) { echo "selected"; } ?> value="<?php echo $categoryDetail['id'];?>"><?php echo $categoryDetail['name'];?></option>
                                <?php
                                endforeach;?>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Specs</label>
                            <textarea type="text" class="form-control" id="specs" name="specs" ><?php echo $product['description'];?></textarea>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-link" href="">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
