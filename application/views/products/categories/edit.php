
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Products Category- Edit</div>

                <div class="panel-body">
                    <form action="" method="POST">
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="categoryID" value="<?php echo $category['id'];?>">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $category['name'];?>">
                        </div>



                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-link" href="">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
