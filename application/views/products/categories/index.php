
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Product Categories
                    <div class="pull-right">

                        <a href="<?php echo base_url().'crm/products/categories/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
                <div class="panel-body">


                <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
        if (!empty($categories)):
            $t=1;
                    foreach ($categories as $category):
                        echo'
                        <tr>
                            <td>'.$t++.'</td>
                            <td>'.$category['name'].'</td>
                            <td>
                                <a href="'.base_url().'crm/products/categories/edit/'.$category['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
                    endforeach;
        else:
            echo '<tr>
    <td colspan="3" align="center">No data Available</td>
</tr>';
        endif;
        ?>

                    </tbody>
                </table>

                </div>
            </div>
        </div>
