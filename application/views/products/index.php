
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Products
                    <div class="pull-right">

                        <a href="<?php echo base_url().'crm/products/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
                <div class="panel-body">
                    <!--<form method="GET">
                        <div class="form-group" style="margin:0">
                            <div class="input-group">
                                <?php if(!empty($keyword)): ?>
                                <span class="input-group-btn">
                                    <a href="<?php echo base_url().'crm/products';?>" class="btn btn-primary">Clear</a>
                                </span>
                                <?php endif;?>
                                <input type="text" name="q" class="form-control" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="submit">Search</button>
                                </span>
                            </div>
                        </div>
                    </form>-->
<br/>
                <table id="data-table" class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Price</th>
							<th>Min Price</th>
                            <th>Quantity</th>
							<th>Category</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
        if(!empty($products)):
             $t=1;
            foreach ($products as $product):

                       echo '<tr>
                             <td>'.$t++.'</td>
                            <td>'.$product['name'].'</td>
                            <td>'.$product['price'].'</td>
							<td>'.$product['min_price'].'</td>
							
                            <td>'.$product['quantity'].'</td>
							  <td>'.$this->crm_model->getDataById('categories',$product['category'])['name'].'</td>
                            <td>';
                echo form_open(base_url() . 'crm/common/delete', array(
                    'method' => 'post',
                    'id' => '',
                    'class'=>'form-inline'
                ));

                               echo '<input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="action" value="products">
                                     <input type="hidden" name="db" value="products">
                                   <input type="hidden" name="rowID" value="'.$product['id'].'">
                                    <button class="btn btn-danger btn-xs pull-right btn-delete" name="delete" type="submit">Delete</button>
                                </form>
                               <a href="'.base_url().'crm/products/edit/'.$product['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
            endforeach;
        else:
            echo '<tr>
    <td colspan="7" align="center">No data Available</td>
</tr>';
        endif;
                    ?>

                    </tbody>
                </table>
                </div>

            </div>
        </div>
