<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 4:17 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SofaZetu Furnitures | No.1 Online store for your Furniture Needs</title>

    <!-- Styles -->

    <link href="<?php echo base_url();?>public/css/app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/all.css" rel="stylesheet">

    <link href="<?php echo base_url();?>public/css/bootstrap.datetimepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>public/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>public/css/jquery-ui.css" rel="stylesheet">


</head>
