<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 4:18 PM
 */
?>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="<?php echo base_url();?>crm/sales/create">
             <img src="<?php echo base_url();?>public/image/SofaZetu-Logo.png" width="100px" height="30px">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
<?php if($this->crm_model->check_permissions('sale','create')): ?>
                <li><a href="<?php echo base_url();?>crm/sales/create">POS</a></li>
				<?php endif;
				if ($this->crm_model->check_permissions('report','daily')): ?>
                <li><a href="<?php echo base_url();?>crm/sales/daily">Sales</a></li>
			<?php endif;
				if ($this->crm_model->check_permissions('customer','index')): ?>
                <li><a href="<?php echo base_url();?>crm/customers">Customers</a></li>
				<?php endif;
				if($this->crm_model->check_permissions('vendor','index')):?>
                <li><a href="<?php echo base_url();?>crm/vendors">Vendors</a></li>
				<?php endif;
				if($this->crm_model->check_permissions('product','index')):?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url();?>crm/products">All Products</a></li>
                        <li><a href="<?php echo base_url();?>crm/products/categories">Product Categories</a></li>

                    </ul>

                </li>
				<?php endif;
				if($this->crm_model->check_permissions('tracking','index')):?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Inventories <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="<?php echo base_url();?>crm/inventories/receivings">Receivings</a></li>
                        <li><a href="<?php echo base_url();?>crm/inventories/adjustments">Adjustments</a></li>-->
                        <li><a href="">Trackings</a></li>
                    </ul>
                </li>
				<?php endif;
				if($this->crm_model->check_permissions('report','daily')):?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Reporting <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if($this->crm_model->check_permissions('report','daily')):?>
                        <li><a href="<?php echo base_url();?>crm/sales/daily">Daily Sales</a></li>
                        <?php endif; if ($this->crm_model->check_permissions('sales','index')):?>
                        <li><a href="<?php echo base_url();?>crm/sales/">Comprehensive Sales</a></li>
                        <?php endif; if ($this->crm_model->check_permissions('home','index')):?>
						<li><a href="<?php echo base_url();?>crm/dashboard/">Dashboard</a></li>
                        <?php endif;?>
                    </ul>
                </li>
<?php endif;
				?>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
<?php
				if($this->crm_model->check_permissions('user','index')):?>

                <li><a href="<?php echo base_url();?>crm/users">Users</a></li>
				<?php endif; ?>
                <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <?php echo $this->session->userdata('UserName');?> <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
					
                        <li><a href="<?php echo base_url();?>crm/settings/profile">Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="<?php echo base_url();?>crm/logout">

                                Logout
                            </a>

                            <form id="logout-form" action="<?php echo base_url();?>crm/logout" method="POST" style="display: none;">

                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
