
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Vendors
                    <div class="pull-right">
                        <a href="<?php echo base_url().'crm/vendors/create';?>" class="btn btn-primary btn-xs">Create</a>
                    </div>
                </div>
                <div class="panel-body">

                <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th width="170"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
        if(!empty($vendors)):
            $t=1;
                    foreach($vendors as $vendor):
                        echo '<tr>
                            <td>'.$t++.'</td>
                            <td>'.$vendor['name'].'</td>
                            <td>'.$vendor['company_name'].'</td>
                            <td>'.$vendor['email'].'</td>
                            <td>'.$vendor['phone'].'</td>
                            <td>';
                        echo form_open(base_url() . 'crm/common/delete', array(
                            'method' => 'post',
                            'id' => '',
                            'class'=>'form-inline'
                        ));
                               echo '<input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="action" value="vendors">
                                     <input type="hidden" name="db" value="suppliers">
                                   <input type="hidden" name="rowID" value="'.$vendor['id'].'">
                                    <button class="btn btn-danger btn-xs pull-right btn-delete" name="delete" type="submit">Delete</button>
                                </form>
                                <a href="'.base_url().'crm/vendors/edit/'.$vendor['id'].'" class="btn btn-primary btn-xs pull-right">Edit</a>
                            </td>
                        </tr>';
                    endforeach;
        else:
           echo '<tr>
    <td colspan="" align="center">No data Available</td>
</tr>';
        endif;
        ?>


</tbody>
                </table>
                </div>

            </div>
        </div>
