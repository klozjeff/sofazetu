<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 5:36 PM
 */

//include  'partials/header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SofaZetu Furnitures | No.1 Online store for your Furniture Needs</title>

    <!-- Styles -->

    <link href="<?php echo base_url();?>public/css/login.css" rel="stylesheet">


</head>
<body class="auth-wrapper">
<div class="all-wrapper with-pattern">
    <div class="auth-box-w">
        <div class="logo-w"><a href="index.html"><img alt="" src="<?php echo base_url();?>public/image/SofaZetu-Logo.png" width="150px" height="50px"></a></div>
        <h4 class="auth-header">Login</h4>
        <?php
            echo form_open(base_url() . 'index.php/crm/login/', array(
                'method' => 'post',
                'id' => 'login',
                'class'=>'form'
            ));
            ?>
            <div class="form-group"><label for="">Email Address</label><input class="form-control"
                                                                         placeholder="Enter your email Address" name="email" id="email" type="email">
                <div class="pre-icon os-icon os-icon-user-male-circle"></div>
            </div>
            <div class="form-group"><label for="">Password</label><input class="form-control"
                                                                         placeholder="Enter your password"
                                                                        name="password" id="password"  type="password">
                <div class="pre-icon os-icon os-icon-fingerprint"></div>
            </div>
            <div class="buttons-w">
                <button class="btn btn-primary">Log me in</button>
               <!-- <div class="form-check-inline"><label class="form-check-label"><input class="form-check-input"
                                                                                      type="checkbox">Forgot Password?
                    Me</label></div>-->
            </div>
        </form>
    </div>
</div>
</body>
<!--
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 login-bg-left">
        </div>
        <div class="col-md-6 login-bg-right">
            <h2>Login</h2>
            <?php
            echo form_open(base_url() . 'index.php/crm/login/', array(
                'method' => 'post',
                'id' => 'login',
                'class'=>'form'
            ));
            ?>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="" autofocus>

                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password">


                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>-->


