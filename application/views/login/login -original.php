<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 5:36 PM
 */

//include  'partials/header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SofaZetu Furnitures | No.1 Online store for your Furniture Needs</title>

    <!-- Styles -->

    <link href="<?php echo base_url();?>public/css/app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>public/css/all.css" rel="stylesheet">
    <!--<link href="<?php echo base_url();?>public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '{{ $google_analytics_id }}', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 login-bg-left">
        </div>
        <div class="col-md-6 login-bg-right">
            <h2>Login</h2>
            <?php
            echo form_open(base_url() . 'index.php/crm/login/', array(
                'method' => 'post',
                'id' => 'login',
                'class'=>'form'
            ));
            ?>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="" autofocus>

                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password">


                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


