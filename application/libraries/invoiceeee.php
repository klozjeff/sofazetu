<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/19/2017
 * Time: 9:36 PM
 */
require_once FCPATH .'/vendor/fpdf/fpdf-1.8.php';
require_once FCPATH .'/vendor/rotation/rotation.php';
class Invoicer extends FPDF_rotation
{

    //original invoice variables
    var $font = 'helvetica';
    var $columnOpacity = 0.06;
    var $columnSpacing = 0.3;
    var $referenceformat = array('.',',');
    var $margins = array('l'=>20,'t'=>20,'r'=>20);

    var $l;
    var $document;
    var $type;
    var $reference;
    var $logo;
    var $color;
    var $date;
    var $due;
    var $from;
    var $to;
    var $ship; // ADDED SHIPPING
    var $items;
    var $totals;
    var $badge;
    var $addText;
    var $footernote;
    var $footernoteLink;
    var $dimensions;
    var $footerdimensions;
    var $lpo;
    var $info;
    var $FPDF;
    var $isFinished;
    function __construct($orientation = 'P', $currency='Kshs', $language='en',$unit = 'mm', $size = 'A5')
    {
        parent::__construct($orientation, $unit, $size);
      //  $this->FPDF = new FPDF();
        $this->columns = 4;
        $this->items = array();
        $this->totals = array();
        $this->addText = array();
        $this->firstColumnWidth = 70;
        $this->currency = $currency;
        $this->maxImageDimensions = array(1000,1000);

         $this->setLanguage($language);
        $this->setDocumentSize($size);
        $this->setColor("#222222");

        $this->FPDF=new FPDF($orientation,$unit,array($this->document['w'],$this->document['h']));
        $this->AliasNbPages();
        $this->SetMargins($this->margins['l'],$this->margins['t'],$this->margins['r']);
    }

    function setType($title)
    {
        $this->title = $title;
    }

    function setColor($rgbcolor)
    {
        $this->color = $this->hex2rgb($rgbcolor);
    }

    function setDate($date)
    {
        $this->date = $date;
    }

    function setDue($date)
    {
        $this->due = $date;
    }


    function setFrom($data)
    {
        $this->from = array_filter($data);
        //print_r(array_filter($data));
    }

    function setTo($data)
    {
        $this->to = $data;
    }

    function shipTo($data)
    {
        $this->ship = $data;
    }

    function setReference($reference)
    {
        $this->reference = $reference;
    }
    function setLPO($lpo)
    {
        $this->lpo = $lpo;
    }
    function setNumberFormat($decimals,$thousands_sep)
    {
        $this->referenceformat = array($decimals,$thousands_sep);
    }

    function flipflop()
    {
        $this->flipflop = true;
    }

    function addItem($item,$description,$quantity,$vat,$price,$discount=0,$total)
    {
        $p['item'] 			= $item;
        $p['description'] 	= $this->br2nl($description);
        $p['vat']			= $vat;
       /* if(is_numeric($vat)) {
            $p['vat']		= $this->currency.' '.number_format($vat,2,$this->referenceformat[0],$this->referenceformat[1]);
        }*/
        $p['quantity'] 		= $quantity;
        $p['price']			= $price;
        $p['total']			= $total;

       /* if($discount!==false) {
            $this->firstColumnWidth = 58;
            $p['discount'] = $discount;
            if(is_numeric($discount)) {
                $p['discount']	= $this->currency.' '.number_format($discount,2,$this->referenceformat[0],$this->referenceformat[1]);
            }
            $this->discountField = true;
            $this->columns = 6;
        }*/

        $this->items[]		= $p;
    }

    function addTotal($name,$value,$colored=0)
    {
        $t['name']			= $name;
        $t['value']			= $value;
        if(is_numeric($value)) {
            $t['value']			= $this->currency.' '.number_format($value,2,$this->referenceformat[0],$this->referenceformat[1]);
        }
        $t['colored']		= $colored;
        $this->totals[]		= $t;
    }
    function setInfo($info)
    {
        $this->info = $info;
    }
    function addTitle($title)
    {
        $this->addText[] = array('title',$title);
    }

    function addParagraph($paragraph)
    {
        $paragraph = $this->br2nl($paragraph);
        $this->addText[] = array('paragraph',$paragraph);
    }

    function addBadge($badge)
    {
        $this->badge = $badge;
    }
    //Function to Set Logo
    function setLogo($logo,$maxWidth=0,$maxHeight=0)
    {
        if($maxWidth and $maxHeight) {
            $this->maxImageDimensions = array($maxWidth,$maxHeight);
        }
        $this->logo = $logo;
        $this->dimensions = $this->resizeToFit($logo);
    }
    function setFooternote($note)
    {


        $this->footernote = $note;

    }
    function setFooternoteLink($link)
    {
        $this->footernoteLink = $link;
    }

    function render($name='',$destination='')
    {
        $this->FPDF->SetAutoPageBreak(true,50);
        $this->FPDF->AddPage();
        $this->Header();
        $this->Body();
        $this->Footer();
        $this->FPDF->AliasNbPages();
        $this->isFinished = true;
        $this->FPDF->Output($name,$destination);

    }

    function Header()
    {
        //First page
        if($this->FPDF->PageNo()==1)
        {
			  if(isset($this->flipflop))
            {
                $to = 'To';
                $from = 'From';
                $ship = 'Address'; // ADDED SHIPPING

                $this->l['to'] = $from;
                $this->l['from'] = $to;
                $this->l['ship'] = $from; // ADDED SHIPPING

                $to = 'To';
                $from = 'From';
                $ship = 'Address';// ADDED SHIPPING

                $this->to = $from;
                $this->from = $to;
                $this->ship = $from; // ADDED SHIPPING
            }
			//New Header

if(isset($this->logo)) {
	
	    	$this->FPDF->Image($this->logo,$this->margins['l'],$this->margins['t'],$this->dimensions[0],$this->dimensions[1]);
	    }

	    //Title
		$this->FPDF->SetTextColor(153,204,255);
		$this->FPDF->SetFont($this->font,'B',12);
	    $this->FPDF->Cell(0,5,iconv("UTF-8", "ISO-8859-1",strtoupper($this->title)),0,1,'C');
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Ln(5);
		
		$lineheight = 5;
		//Calculate position of strings
		$this->FPDF->SetFont($this->font,'B',9);	
		$positionX = $this->document['w']-$this->margins['l']-$this->margins['r']-max(strtoupper($this->FPDF->GetStringWidth($this->l['number'])),strtoupper($this->FPDF->GetStringWidth($this->l['date'])),strtoupper($this->FPDF->GetStringWidth($this->l['due'])))-35;
		
	    //Company Name
	    $this->FPDF->Cell($positionX,$lineheight);
	    /*
	    $this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['number']).':'),0,0,'L');*/
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->from[0],0,1,'L');
		
		//Company Box No
		$this->FPDF->Cell($positionX,$lineheight);
		/*
		$this->FPDF->SetFont($this->font,'B',9);
		$this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['date'])).':',0,0,'L');*/	
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->from[1],0,1,'L');
		
		//Company Phone No
		$this->FPDF->Cell($positionX,$lineheight);
		/*
		$this->FPDF->SetFont($this->font,'B',9);
		$this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['date'])).':',0,0,'L');*/	
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->from[2],0,1,'L');
		
		//Company Email
		$this->FPDF->Cell($positionX,$lineheight);
		/*
		$this->FPDF->SetFont($this->font,'B',9);
		$this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['date'])).':',0,0,'L');*/	
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->from[3],0,1,'L');
		
		//Company Website
		$this->FPDF->Cell($positionX,$lineheight);
		/*
		$this->FPDF->SetFont($this->font,'B',9);
		$this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['date'])).':',0,0,'L');*/	
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->from[4],0,1,'L');
		
		//Invoice No
		$this->FPDF->Cell($positionX,$lineheight);
		
		$this->FPDF->SetFont($this->font,'B',9);
		$this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
		$this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",strtoupper($this->l['number2'])).':',0,0,'L');	
		$this->FPDF->SetTextColor(50,50,50);
		$this->FPDF->SetFont($this->font,'',9);
		$this->FPDF->Cell(0,$lineheight,$this->reference,0,1,'L');
		
		//END New Header
		  $this->FPDF->Ln();
            $this->FPDF->SetLineWidth(0.3);
            $this->FPDF->SetDrawColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->Line($this->margins['l'], $this->FPDF->GetY(),$this->document['w']-$this->margins['r'], $this->FPDF->GetY());
            $this->FPDF->Ln(4);


            if(($this->margins['t']+$this->dimensions[1]) > $this->FPDF->GetY())
            {
                $this->FPDF->SetY($this->margins['t']+$this->dimensions[1]+5);
            }
            else
            {
                $this->FPDF->SetY($this->FPDF->GetY()+5);
            }
           
            /*Title
            $this->FPDF->SetTextColor(153,204,255);
            $this->FPDF->SetFont('helvetica','B',12);
            $this->FPDF->Cell(0,5,iconv("UTF-8", "ISO-8859-1",strtoupper($this->title)),0,1,'C');
            $this->FPDF->SetFont($this->font,'',9);
            $this->FPDF->Ln(5);
            $lineheight = 5;
*/

            $this->FPDF->Ln(5);
            $this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->SetDrawColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->SetFont($this->font,'B',10);
            $width = $this->document['w']-$this->margins['l']-$this->margins['r']-100;
          


            //Calculate position of strings
            $this->FPDF->SetFont($this->font,'B',9);
            $positionX = $this->document['w']-$this->margins['l']-$this->margins['r']-max(strtoupper($this->FPDF->GetStringWidth($this->l['number'])),strtoupper($this->FPDF->GetStringWidth($this->l['date'])),strtoupper($this->FPDF->GetStringWidth($this->l['due'])))-35;

            //Number
            $this->FPDF->Cell($positionX,$lineheight);
            $this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",'Purchase Order #:'),0,0,'L');
            $this->FPDF->SetTextColor(50,50,50);
            $this->FPDF->SetFont($this->font,'',9);
            $this->FPDF->Cell(0,$lineheight,'XIA/'.date('Y').'/'.$this->reference,0,1,'R');

            //Date
            $this->FPDF->Cell($positionX,$lineheight);
            $this->FPDF->SetFont($this->font,'B',9);
            $this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->l['date']).':',0,0,'L');
            $this->FPDF->SetTextColor(50,50,50);
            $this->FPDF->SetFont($this->font,'',9);
            $this->FPDF->Cell(0,$lineheight,$this->date,0,1,'R');

            //LPO Number
                $this->FPDF->Cell($positionX,$lineheight);
                $this->FPDF->SetFont($this->font,'B',9);
                $this->FPDF->SetTextColor($this->color[0],$this->color[1],$this->color[2]);
                $this->FPDF->Cell(32,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->l['due'].':'),0,0,'L');
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->SetFont($this->font,'',9);
                $this->FPDF->Cell(0,$lineheight,$this->lpo,0,1,'R');
           





            //Information
            if(($this->margins['t']+$this->dimensions[1]) > $this->FPDF->GetY())
            {
                $this->FPDF->SetY($this->margins['t']+$this->dimensions[1]-15);
            }
            else
            {
                $this->FPDF->SetY($this->FPDF->GetY()-15);
            }
            $this->FPDF->Cell($width,$lineheight,$this->l['to'],0,0,'L');
            $this->FPDF->Cell($width,$lineheight,'',0,0,'L');
            $this->FPDF->Cell(0,$lineheight,'',0,0,'L'); // ADDED SHIPPING
            $this->FPDF->Ln(7);
            $this->FPDF->SetTextColor(50,50,50);
            $this->FPDF->SetFont($this->font,'B',10);
            $this->FPDF->Cell($width,$lineheight,$this->to[0],0,0,'L');
            $this->FPDF->Cell($width,$lineheight,'',0,0,'L');
            $this->FPDF->Cell(0,$lineheight,'',0,0,'L'); // ADDED SHIPPING
            $this->FPDF->SetFont($this->font,'',8);
            $this->FPDF->SetTextColor(100,100,100);
            $this->FPDF->Ln(5);
            $this->FPDF->Cell($width,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->to[1]),0,1,'L');
			$this->FPDF->SetFont($this->font,'',8);
            $this->FPDF->SetTextColor(100,100,100);
            $this->FPDF->Cell($width,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->to[2]),0,1,'L');
            $this->FPDF->Ln(3);
            /*  $this->FPDF->Cell($width,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->to[3]),0,2,'L');*/

            /*Attention:
           $this->FPDF->SetTextColor(50,50,50);
           $this->FPDF->SetFont($this->font,'B',10);
           $this->FPDF->Cell($width,$lineheight,$this->l['attention'],0,0,'L');
           $this->FPDF->Cell($width,$lineheight,'',0,0,'L');
           $this->FPDF->Cell(0,$lineheight,'',0,0,'L'); // ADDED SHIPPING
           $this->FPDF->SetFont($this->font,'',9);
           $this->FPDF->SetTextColor(100,100,100);
           $this->FPDF->Ln(5);
           $this->FPDF->Cell($width,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->to[7]),0,1,'L');
           $this->FPDF->Cell($width,$lineheight,iconv("UTF-8", "ISO-8859-1",$this->to[8]),0,1,'L');
           $this->FPDF->Ln(4);
          Customer Notes or RE

           $this->FPDF->SetTextColor(50,50,50);
           $this->FPDF->SetFont($this->font,'',10);
           $this->FPDF->MultiCell(0,4,iconv("UTF-8", "ISO-8859-1",'RE:'.$this->info),0,'L',0);
           $this->FPDF->Ln(4);

*/
            ///Ref No,Date,LPO No.

            if(($this->margins['t']+$this->dimensions[1]) > $this->FPDF->GetY())
            {
                $this->FPDF->SetY($this->margins['t']+$this->dimensions[1]-25);
            }
            else
            {
                $this->FPDF->SetY($this->FPDF->GetY()-25);
            }
            $lineheight = 5;

        }
        $this->FPDF->Ln(5);


    }

    function Body()
    {

        if(($this->margins['t']+$this->dimensions[1]) > $this->FPDF->GetY())
        {
            $this->FPDF->SetY($this->margins['t']+$this->dimensions[1]+45);

        }
        else
        {
            $this->FPDF->SetY($this->FPDF->GetY()+22);

        }
        $header=array('S/No','Item Description','Qty','Price','Sub-Total');
        $this->FPDF->SetFillColor(224,235,255);
        $this->FPDF->SetTextColor(0);
        $this->FPDF->SetLineWidth(0.3);
        $this->FPDF->SetDrawColor(102,205,170);
        $this->FPDF->SetFont('Helvetica','B');
        //Header
        $width_other_serial = ($this->document['w']-$this->margins['l']-$this->margins['r']-$this->firstColumnWidth-($this->columns*$this->columnSpacing))/($this->columns+1);

        $width_other = ($this->document['w']-$this->margins['l']-$this->margins['r']-$this->firstColumnWidth-($this->columns*$this->columnSpacing))/($this->columns-1);
        $cellHeight = 9;
        $bgcolor = 255;
        $cHeight = $cellHeight;
        $w= ($this->document['w']-$this->margins['l']-$this->margins['r']-$this->firstColumnWidth-($this->columns*$this->columnSpacing))/($this->columns-1);
        for($i=0;$i<count($header);$i++)
        {
            if($i==0)
                $this->FPDF->Cell($width_other_serial,9,strtoupper($header[0]),1,0,'C',true);
            else if($i==1)
                $this->FPDF->Cell($this->firstColumnWidth,9,strtoupper($header[1]),1,0,'C',true);
            else
                $this->FPDF->Cell($w,9,strtoupper($header[$i]),1,0,'C',true);
        }
        $this->FPDF->Ln();
        //Color & Font Restoration
        $this->FPDF->SetFillColor(224,235,255);
        $this->FPDF->SetTextColor(0);
        $this->FPDF->SetFont('');

        //Data
        $fill=false;
        $t=0;
        if($this->items)
        {
            foreach($this->items as $item)
            {


                $t+=1;

                $this->FPDF->SetFont($this->font,'',8);
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->Cell($width_other_serial,$cHeight,$t,'LR',0,'L',$fill);

                //column 1
                $this->FPDF->SetFont($this->font,'',8);
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->Cell($this->firstColumnWidth,$cHeight,iconv("UTF-8", "ISO-8859-1",$item['item']),'LR',0,'L',$fill);


                //Column 2
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->SetFont($this->font,'',8);
                $this->FPDF->Cell($w,$cHeight,$item['quantity'],'LR',0,'C',$fill);





                //Column 3

                $this->FPDF->Cell($w,$cHeight,iconv('UTF-8', 'windows-1252', $this->currency.' '.number_format($item['price'],2,$this->referenceformat[0],$this->referenceformat[1])),'LR',0,'C',$fill);
                //Column 4

               /* $this->FPDF->Cell($w,$cHeight,iconv('UTF-8', 'windows-1252', $item['vat']),'LR',0,'R',$fill);

                //Column 5
                if(isset($this->discountField))
                {

                    if(isset($item['discount']))
                    {
                        $this->FPDF->Cell($w,$cHeight,iconv('UTF-8', 'windows-1252',$item['discount']),'LR',0,'C',$fill);
                    }
                    else
                    {
                        $this->FPDF->Cell($w,$cHeight,'',0,0,'C',$fill);
                    }
                }*/

                //Column 6

                $this->FPDF->Cell($w,$cHeight,iconv('UTF-8', 'windows-1252', $this->currency.' '.number_format($item['total'],2,$this->referenceformat[0],$this->referenceformat[1])),'LR',0,'C',$fill);
                $this->FPDF->Ln();
                $fill=!$fill;
                $this->FPDF->SetLineWidth(0.3);
                $this->FPDF->SetDrawColor(102,205,170);
                $this->FPDF->Line($this->margins['l'], $this->FPDF->GetY(),$this->document['w']-$this->margins['r']+14, $this->FPDF->GetY());

            }




        }

        //Add totals
        if($this->totals)
        {

            $this->FPDF->SetFillColor(255,255,255);
            $this->FPDF->SetTextColor(0);
            $this->FPDF->SetLineWidth(0.3);
            $this->FPDF->SetDrawColor(102,205,170);
            $this->FPDF->SetFont('');
            $fillTotal=false;
            foreach($this->totals as $total)
            {
                $this->FPDF->Cell($width_other_serial,$cellHeight,'',0,0,'L',0);
                $this->FPDF->Cell($this->firstColumnWidth,$cellHeight,'',0,0,'L',0);
                for($i=0;$i<$this->columns-5;$i++)
                {
                    $this->FPDF->Cell($width_other,$cellHeight,'',0,0,'R',0);
                }

                if($total['colored'])
                {
                    $this->FPDF->SetTextColor(0,0,0);
                    $this->FPDF->SetFillColor(224,235,255);
                    $this->FPDF->SetDrawColor(102,205,170);
                }
                //column 1
                $this->FPDF->SetFont($this->font,'B',8);
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->Cell($width_other,$cellHeight,iconv("UTF-8", "ISO-8859-1",$total['name']),1,0,'L',1);


                //Column 2
                $this->FPDF->SetTextColor(50,50,50);
                $this->FPDF->SetFont($this->font,'B',8);
                $this->FPDF->SetFillColor($bgcolor,$bgcolor,$bgcolor);
                if($total['colored'])
                {

                    $this->FPDF->SetTextColor(0,0,0);
                    $this->FPDF->SetFillColor(224,235,255);
                    $this->FPDF->SetDrawColor(102,205,170);
                }
                $this->FPDF->Cell($width_other,$cellHeight,$total['value'],1,0,'C',1);
                $this->FPDF->Ln();


            }


        }
        $this->FPDF->Ln();
        $this->productsEnded = false;
        $this->FPDF->Ln();
        $this->FPDF->Ln(3);

    }


    function Footer()
    {
        $totalPa='{nb}';
        $this->FPDF->SetY(-15);
        $this->FPDF->SetFont($this->font,'',10);
        $this->FPDF->SetTextColor(50,50,50);

            $width = ($this->document['w']-$this->margins['l']-$this->margins['r'])/2;
            $lineheight=5;
            $this->FPDF->Cell($width,$lineheight,'Sofazetu Limited-'.date('Y'),0,0,'L');
            $this->FPDF->Cell($width,$lineheight,$this->l['accounts'],0,0,'R');

            $this->FPDF->Ln();
            $this->FPDF->SetLineWidth(0.3);
            $this->FPDF->SetDrawColor($this->color[0],$this->color[1],$this->color[2]);
            $this->FPDF->Line($this->margins['l'], $this->FPDF->GetY(),$this->document['w']-$this->margins['r'], $this->FPDF->GetY());
            $this->FPDF->Ln(4);
            if(isset($this->footernote)) {
                $this->FPDF->Cell(0,10,$this->footernote,0,0,'L');
            }




      //  $this->FPDF->SetFont($this->font,'',8);
      //  $this->FPDF->SetTextColor(50,50,50);
        //$this->FPDF->Cell(0,10,$this->footernoteLink,0,0,'L');
       // $this->FPDF->Cell(0,10,$this->l['page'].' '.$this->PageNo().' '.$this->l['page_of'].' '.$totalPa,0,0,'R');

    }
    /*******************************************************************************
     *                                                                              *
     *                               Private methods                                *
     *                                                                              *
     *******************************************************************************/
    private function setLanguage($language)
    {
        $this->language = $language;
        //include('language/'.$language.'.inc');
        include (FCPATH .'/application/language/en.php');
        $this->l = $l;
    }

    private function setDocumentSize($dsize)
    {
        switch ($dsize)
        {
            case 'A4':
                $document['w'] = 210;
                $document['h'] = 297;
                break;
            case 'letter':
                $document['w'] = 215.9;
                $document['h'] = 279.4;
                break;
            case 'legal':
                $document['w'] = 215.9;
                $document['h'] = 355.6;
                break;
            default:
                $document['w'] = 210;
                $document['h'] = 297;
                break;
        }
        $this->document = $document;
    }

    private function resizeToFit($image)
    {
        list($width, $height) = getimagesize($image);
        $newWidth = $this->maxImageDimensions[0]/$width;
        $newHeight = $this->maxImageDimensions[1]/$height;
        $scale = min($newWidth, $newHeight);
        return array(
            round($this->pixelsToMM($scale * $width)),
            round($this->pixelsToMM($scale * $height))
        );
    }

    private function pixelsToMM($val)
    {
        $mm_inch = 25.4;
        $dpi = 96;
        return $val * $mm_inch/$dpi;
    }

    private function hex2rgb($hex)
    {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        return $rgb;
    }

    private function br2nl($string)
    {
        return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
    }

}