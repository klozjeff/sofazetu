<?php

class Pdf {

   public function __construct() {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
    function load($param = NULL) {
	
      define("_MPDF_TEMP_PATH", FCPATH .'public/pdf_temp');
	  require_once FCPATH .'/vendor/mpdf/mpdf/mpdf.php';
    
        if ($param == NULL) {
            $param = '"en-GB-x","A5","","",10,10,10,10,6,3';
        }
		return new mPDF($param);
  
    }

}