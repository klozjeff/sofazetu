<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 4:37 PM
 */
if(!defined('BASEPATH'))
    exit('No direct script access allowed');
class Crm_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function saveData($table,$data)
    {
        if ($data):
        $name = $data['name'];
        $this->db->select("* from $table where name='$name'");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->insert($table, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {

            return false;
        }

        endif;

    }
    function saveDataRID($table,$data)
    {
        if ($data):
            $name = $data['name'];
            $this->db->select("* from $table where name='$name'");
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() == 0) {
                $this->db->insert($table, $data);
                if ($this->db->affected_rows() > 0) {
                    return $this->db->insert_id();
                }
            } else {

                $queryArray=$query->result_array();
                foreach ($queryArray as $d):
                    return $d['id'];
                endforeach;
            }

        endif;

    }


    function saveDataID($table,$data)
    {
        if ($data):
            $saleID = $data['saleID'];
            $this->db->select("* from $table where uniqueID='$saleID'");
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() == 0) {
                $this->db->insert($table, $data);
                if ($this->db->affected_rows() > 0) {
                    return $this->db->insert_id();
                }
            } else {
                $queryArray=$query->result_array();
                foreach ($queryArray as $d):
                  return $d['id'];
                endforeach;
            }

        endif;

    }
    function getDataById($table,$DataID)
    {
        if($DataID):
            $this->db->where('id',$DataID);
            $dataDetails=$this->db->get($table)->result_array();
            foreach ($dataDetails as $dataDetail):
                return $dataDetail;
            endforeach;
        endif;


    }

    function getSelectOptions($table,$field)
    {
        if ($table):
		
		 $this->db->select($field);
        $this->db->from($table);
        $this->db->order_by("$field", "asc");
        $query = $this->db->get()->result_array();

        if($query):
            foreach ($query as $row):
  return "<option value=".$row['id'].">".$row["$field"]."</option>";
              
            endforeach;
           
      
        endif;
		endif;
          /*  $dataDetails=$this->db->get($table)->result_array();
            foreach ($dataDetails as $dataDetail):
                return "<option value=".$dataDetail['id'].">".$dataDetail["$field"]."</option>";
            endforeach;
        endif;*/
    }


    function is_email_exists($table,$email, $id = 0) {

        $result = $this->db->get_where($table, array('email' => $email));
        if ($result->num_rows() && $result->row()->RowID != $id) {
            return $result->row();
        } else {
            return false;
        }
    }


    function getSaleData($table,$saleID)
    {
        if ($table):
            $dataDetails=$this->db->get_where($table, array('id' => $saleID))->result_array();
            foreach ($dataDetails as $dataDetail):
                return $dataDetail;
            endforeach;
        endif;
    }

    function getSaleItems($table,$saleID,$type)
    {
        if ($table):
            $dataDetails=$this->db->get_where($table, array('sale_id' => $saleID,'item_type'=>$type))->result_array();
           return $dataDetails;
        endif;
     }
function getLastSaleID()
{
    $this->db->select('uniqueID');
    $this->db->from('sales');
    $this->db->order_by("uniqueID", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()==0)
    {
        return 100;
    }
    else {
        $vv = $query->row()->uniqueID + 1;
        if (strlen($vv) == 2):
            return '0' . $vv;
        else:
            return $vv;
        endif;
    }
}

    function popProductsList(){
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('deleted_at',null);
        $this->db->order_by("name", "asc");
        $query = $this->db->get()->result_array();

        if($query):
            echo '<select class="form-control item-select">';
            foreach ($query as $row):

                print '<option value="'.$row['price'].'">'.$row["name"].'</option>';
            endforeach;
            echo '</select>';
        else:
            echo "<p>There are no products to display.</p>";
        endif;
    }

    public function delete_row($table,$field,$id){
        $this -> db -> where($field, $id);
        $this -> db -> delete($table);
    }
	
	
    function getData($table,$dataID)
    {
        if ($table):
            $dataDetails=$this->db->get_where($table, array('id' => $dataID))->result_array();
            foreach ($dataDetails as $dataDetail):
                return $dataDetail;
            endforeach;
        endif;
    }
	
	 function get_type_name_by_id($type, $type_id = '', $field = '')
    {
	
        if ($type_id != '') {
            $l = $this->db->get_where($type, array(
                'role_id' => $type_id
            ));
            $n = $l->num_rows();
            if ($n > 0) {
                return $l->row()->$field;
            }
        }
    }
	
	function check_permissions($obj,$act)
	{
		 if ($this->session->userdata('Login') !=true) {
            return false;
        }
		
		$user_id   = $this->session->userdata('UserID');
		$role_id   = $this->session->userdata('Role');
        
		$user= $this->db->get_where('users', array(
            'id' => $user_id
        ))->row();
		 
		/* $l=array();
        $permission = $this->db->get_where('permissions',array('object'=> $obj,'action'=> $act))->result_array();
		foreach ($permission as $vl):
		 $l[]=$vl['id'];
		 endforeach;*/
	
		
        if ($role_id  == 1) {
            return true;
        } else {
          
    $this->db->select('*');
    $this->db->from('permissions');
    $this->db->where("object",$obj);
	$this->db->where("action",$act);
    $controller = $this->db->get()->result_array();
	foreach($controller as $permission):
	$mimi=array();
	$query =$this->db->get_where('permission_role', array('role_id' => $role_id))->result_array();
					foreach($query as $pp):
						$mimi[]=$pp['permission_id'];
					endforeach;
					if(in_array($permission['id'],$mimi)):
					return true;
					else:
					return false;
					endif;
			
			
			endforeach;

		  /*$role = $role_id;
            //$role_permissions =$this->crm_model->get_type_name_by_id('permission_role', $role, 'permission_id');
			 $p= $this->db->get_where('permission_role', array(
                'role_id' => $role
            ));
     
			    $t=array();
               $y=$p->result_array();
		  foreach ($y as $yl):
		 $t[]=$yl['permission_id'];
		 endforeach;
            if (in_array($l, $t)) {
                return true;
            } else {
                return false;
            }*/
        }
		/**/
		
	}

    #Total Sales
    function getSalesTotalByType($type='')
    {
        $this->db->select('*');
        $this->db->where("sale_type!=", "New Sale");
        if(!empty($type)):
        $this->db->where('status',$type);
        endif;
        //$this->db->where('date_payment >=', $min_date);
        // $this->db->where('date_payment <=', $max_date);
        return $this->db->get('sales')->num_rows();
        #End
    }
    #Total Sales by Cashier
    function getSalesByUser($cashier)
    {
        $this->db->select('*');
        $this->db->where("sale_type!=", "New Sale");
        $this->db->where('cashier_id',$cashier);
        return $this->db->get('sales')->num_rows();
        #End
    }
/*
 function popProductsList(){
     $this->db->select('*');
     $this->db->from('products');
     $this->db->where('deleted_at',null);
     $this->db->order_by("name", "asc");
     $query = $this->db->get()->result_array();

     if($query):
         print '<table class="table table-striped table-bordered" id="data-table"><thead><tr>

				<th><h4>Name</h4></th>
				<th><h4>Price</h4></th>
				<th><h4>Category</h4></th>
				<th><h4>Action</h4></th>

			  </tr></thead><tbody>';
     foreach ($query as $row):

         print '
			    <tr>
					<td>'.$row["name"].'</td>
				    <td>'.$row["price"].'</td>
				    <td>'.$row["category"].'</td>
				    <td><a href="#" class="btn btn-primary btn-xs product-select"
				    data-product-id="'.$row['id'].'" data-product-name="'.$row['name'].'" 
				    data-product-price="'.$row['price'].'" data-product-category="'.$row['category'].'">Select</a></td>
			    </tr>
		    ';
         endforeach;
         print '</tr></tbody></table>';
     else:
         echo "<p>There are no products to display.</p>";
         endif;
 }
*/

    /*function getCustomerById($CustomerID)
    {
        if($CustomerID):
            $this->db->where('id',$CustomerID);
            $customerDetails=$this->db->get('customers')->result_array();
            foreach ($customerDetails as $customerDetail):
                return $customerDetail;
            endforeach;
        endif;


    }*/


}