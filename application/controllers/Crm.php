<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/30/2017
 * Time: 3:58 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Crm extends CI_Controller
{
	
	
var $invoicer;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
    }

    /**
     *
     */
    public function index()
    {


            if ($this->session->userdata() && $this->session->userdata('Login')==true):
                $page_data['page_name'] = "dashboard/index";
                $this->load->view('index',$page_data);
            else:
                $this->load->view('login/login');
             endif;


    }

    public function login($para1='')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run()==FALSE)
        {
            echo validation_errors();
        }
        else
        {
            $login_data=$this->db->get_where('users',array(
                'email'=>$this->input->post('email'),
                'password'=>sha1($this->input->post('password'))
            ));
            if($login_data->num_rows()>0)
            {
                foreach($login_data->result_array() as $row)
                {
                    $sessiondata=array(
                        'CrmLogin'=>true,
                        'Login'=>true,
                        'UserID'=>$row['id'],
                        'Online'=>$row['logged_in'],
                        'UserName'=> $row['name'],
                        'Email'=>$row['name'],
						'Role'=>$row['role_id'],
						'Branch'=>$row['branch'],
                    );
                    $this->session->set_userdata($sessiondata);
                    if($row['role_id']==1):
                        $this->session->set_userdata('isAdmin',true);
                    else:
                        $this->session->set_userdata('isUser',true);
                   endif;
                    $loggedin=array(
                        "logged_in" => 1,
                        "last_login" => get_current_utc_time(),
                    );
                    $this->db->where('id', $row['id']);
                    $this->db->update('users', $loggedin);
                    $this->session->set_flashdata('login_success', '<div class="alert alert-success text-center">Login Successful.</div>');
                    if($this->session->userdata('Role')==1):
                    redirect('crm/dashboard');
                    else:
                    redirect('crm/sales/create');
                    endif;
                }

            }

            else
            {
                $this->session->set_flashdata('loginFail', '<div class="alert alert-danger text-center">Incorrect ID Number or password.</div>');

                redirect('crm/index');

            }
        }


    }
    public function logout()
    {
        $loggedin=array(
            "logged_in" => 0,
            "last_login" => get_current_utc_time(),
        );
        $this->db->where('id', $this->session->userdata['UserID']);
        $this->db->update('users', $loggedin);
        $this->session->sess_destroy();
        redirect(base_url() . 'index.php/crm', 'refresh');
    }

    public function isLogged(){
        if ($this->session->userdata('Login')):
            return true;
        else:
            return false;
        endif;
    }

    public function isAdmin(){
        if($this->session->userdata('isAdmin')):
            return true;
        else:
            return false;
        endif;
    }


public function filter($params){
    switch ($params) {
        case 'today':
           return $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d')", date('Y-m-d'));
            break;
        case 'week':
            $previous_week = strtotime("0 week +1 day");

            $start_week = strtotime("last sunday midnight",$previous_week);
            $end_week = strtotime("next saturday",$start_week);

            $start_week = date("Y-m-d",$start_week);
            $end_week = date("Y-m-d",$end_week);
           return $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') >=", $start_week);
            return $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') <=",$end_week);
            break;
        case 'month':
            return $this->db->where("MONTH(created_at)", date('m'));
            break;
        default:

            break;
    }
}
    public function dashboard(){

        $params=$this->input->get('filter');



        #Total Sales No
        $this->db->select('*');
        $this->filter($params);
        $this->db->where("sale_type!=", "New Sale");
        $data['sales_no']=$this->db->get('sales')->num_rows();
        #End

        #Total Customers

        $this->db->select('*');
        $this->filter($params);
        $this->db->group_by('name');
        $data['total_customers']=$this->db->get('customers')->num_rows();
        #End

        #Total Sales Amount
        $this->db->select_sum('sale_total');
        $this->filter($params);
        $this->db->where("sale_type!=", "New Sale");
        $data['total_sales']=$this->db->get('sales')->result_array();
        #End

        #Latest Sales
        $this->db->limit(5);
        $this->db->where("sale_type!=", "New Sale");
        $this->db->order_by("uniqueID", "desc");
        $data['latest_sales']=$this->db->get('sales')->result_array();
        #End
        $this->db->limit(2);
        $data['users']   = $this->db->get_where('users',array('role_id'=>'3'))->result_array();

        $data['page_name']='dashboard/index';
        $this->load->view('index',$data);
    }

    /**
     * @param string $para1
     * @param string $para2
     */
    public function sales($para1='', $para2=''){
       if($para1=='create'):
	    	 if (!$this->crm_model->check_permissions('sale','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
           if($this->input->post()):
               //customer Info
               $sale_customer=array(
                   "name" => $this->input->post('customer_name'),
                   "phone" => $this->input->post('phone'),
                   "address" => $this->input->post('address'),
                   "created_at" => get_current_utc_time()
               );
               $customer= $this->crm_model->saveDataRID('customers',$sale_customer);
               //Sale general Info
               if ($this->input->post('box')):
                   $cc=$this->input->post('box');
                   if (is_array($this->input->post('box'))):
                       $content='';
                      for ($i=0;$i<count($this->input->post('box'));$i++):
                          $content=$content."$cc[$i],";
                          endfor;
                          $accessories=$content;
                       endif;
                   endif;
               $sale_data = array(
                   "customer_id" => $customer,
                   "cashier_id" => $this->session->userdata('UserID'),
                   "sale_branch_id" => $this->input->post('branch_id'),
                   "uniqueID" => $this->input->post('sale_id'),
                   "sale_type" => $this->input->post('sale_type'),
                   "delivery_date" =>$this->input->post('delivery_date'),
                   "sale_deposit" => $this->input->post('pos_deposit'),
                   "sale_balance" =>$this->input->post('pos_balance'),
                   "sale_subtotal" =>$this->input->post('invoice_subtotal'),
                   "sale_accessories_total" =>$this->input->post('invoice_additional'),
                   "sale_transport" =>$this->input->post('invoice_shipping'),
                   "sale_total" =>$this->input->post('invoice_total'),
                   "sale_accessories" =>$accessories,
                   "created_at" => get_current_utc_time()
               );
               $sales= $this->crm_model->saveDataID('sales',$sale_data);

               //Sale Items
               $products=$this->input->post('pos_product');
               foreach ($products as $key => $value):
                   $sale_items[] = array(
                         "sale_id"=>$sales,
                         "product_id"=>$value,
                          "price"=>$this->input->post('pos_product_price')[$key],
                          "quantity"=>$this->input->post('pos_product_qty')[$key],
                       "sub_total"=>$this->input->post('pos_product_sub')[$key],
                       "item_type"=>'products',
                          "created_at" => get_current_utc_time()
                   );
               endforeach;

               if ($sale_items) {
                   $this->db->insert_batch('sale_items', $sale_items);
                }//E# if statement


               if ($this->input->post('box')):
                   $accessories=$this->input->post('box');
               if(in_array('comforters',$accessories))
               {
                   $acc_itemsa=array(
                       "sale_id"=>$sales,
                       "product_id"=>'comforters',
                       "price"=>$this->input->post('pos_price_comforters'),
                       "quantity"=>$this->input->post('pos_qty_comforters'),
                       "sub_total"=>$this->input->post('pos_subtotal_comforters'),
                       "item_type"=>'accessories',
                       "created_at" => get_current_utc_time()
                   );
                   $this->db->insert('sale_items', $acc_itemsa);
               }
                   if(in_array('scatters',$accessories))
                   {
                       $acc_itemsb=array(
                           "sale_id"=>'scatters',
                           "product_id"=>$value,
                           "price"=>$this->input->post('pos_price_scatters'),
                           "quantity"=>$this->input->post('pos_qty_scatters'),
                           "sub_total"=>$this->input->post('pos_subtotal_scatters'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsb);
                   }
                   if(in_array('footrest',$accessories))
                   {
                       $acc_itemsc=array(
                           "sale_id"=>$sales,
                           "product_id"=>'footrest',
                           "price"=>$this->input->post('pos_price_footrest'),
                           "quantity"=>$this->input->post('pos_qty_footrest'),
                           "sub_total"=>$this->input->post('pos_subtotal_footrest'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsc);
                   }
                   if(in_array('others',$accessories))
                   {
                       $acc_itemsd=array(
                           "sale_id"=>$sales,
                           "product_id"=>'others',
                           "price"=>$this->input->post('pos_price_others'),
                           "quantity"=>$this->input->post('pos_qty_others'),
                           "sub_total"=>$this->input->post('pos_subtotal_others'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsd);
                   }
                 /*  $accessories=$this->input->post('box');
               foreach ($accessories as $key):
                $acc_items[]=array(
                    "sale_id"=>$sales,
                    "product_id"=>$value,
                    "price"=>$this->input->post('pos_price_'.$value)[$key],
                    "quantity"=>$this->input->post('pos_qty_'.$value)[$key],
                    "sub_total"=>$this->input->post('pos_subtotal_'.$value)[$key],
                    "item_type"=>'accessories',
                    "created_at" => get_current_utc_time()
                );
                   endforeach;

                       $this->db->insert_batch('sale_items', $acc_items);
*/
               endif;


               echo json_encode(array(
                   'status' => 'Success',
                   'message' => $this->input->post('sale_id')
               ));
            endif;
           $data['page_name']='sales/create';
           $this->load->view('index',$data);
            elseif($para1=='receipt'):
           $data['sales_data'] = $this->crm_model->getSaleData('sales',$para2);
           $data['sales_items'] = $this->crm_model->getSaleItems('sale_items',$para2);
           $this->load->view('sales/receipt',$data);
		   
		 elseif($para1=='purchaseorder'):
          $data['sales_data'] = $this->crm_model->getSaleData('sales',$para2);
           $data['sales_items'] = $this->crm_model->getSaleItems('sale_items',$para2,'products');
           $data['sales_acc'] = $this->crm_model->getSaleItems('sale_items',$para2,'accessories');
           ini_set('memory_limit', '256M');
           // load library
           $this->load->library('fpdf_gen');
           $this->fpdf->SetFont('Arial','B',16);
		   $this->fpdf->Cell(40,10,'Hello World!');
		
		   echo $this->fpdf->Output('hello_world.pdf','D');
       elseif($para1=='testinvoicer'):
           $sales_data = $this->crm_model->getSaleData('sales',$para2);
           $sales_items = $this->crm_model->getSaleItems('sale_items',$para2,'products');
           $sales_acc= $this->crm_model->getSaleItems('sale_items',$para2,'accessories');
           ini_set('memory_limit', '256M');
		   
		   //Delete Old PDF if exists
		  // unlink('PO-'.$para2.'.pdf');
           // load library
           $this->load->library('invoicer');
           $this->invoicer = new invoicer("P","Kshs","en","mm","A4");
           $this->invoicer->setNumberFormat('.',',');
           //Set your logo
           $this->invoicer->setLogo(FCPATH.'/public/image/SofaZetu-Logo.png','356','95');
           //Set theme color
           $this->invoicer->setColor('#222222');
           //Set type
           $this->invoicer->setType('Purchase Order');
           //Set reference
           $this->invoicer->setReference( $sales_data['uniqueID']);
           //Set LPO
           $this->invoicer->setLPO($sales_data['delivery_date']);
           //Set date
           $this->invoicer->setDate(date('Y-m-d',strtotime($sales_data['created_at'])));
           //Set due date
           $this->invoicer->setDue($sales_data['delivery_date']);
           //Set from
           $this->invoicer->setFrom(array('SOFAZETU LIMITED','P.O BOX 60596-00200 NAIROBI','CELL:0729 547 704 /0702 828 402/0790 795 566','Email:sales@sofazetu.co.ke','Website:www.sofazetu.co.ke'));
           //Set to
           $this->invoicer->setTo(array($this->crm_model->getDataById('customers',$sales_data['customer_id'])['name'],$this->crm_model->getDataById('customers',$sales_data['customer_id'])['address'],$this->crm_model->getDataById('customers',$sales_data['customer_id'])['phone']));
           //Ship to
           $this->invoicer->shipTo(array('Sofazetu','Sofazetu','Sofazetu','Sofazetu','Sofazetu','Sofazetu',''));
           //Add items

           foreach($sales_items as $item):

               //	foreach($rows['product'] as $key => $value)
               //	{
               $item_product = $item['product_id'];
               $item_qty = $item['quantity'];
               $item_price = $item['price'];
               $item_discount = '0';
               $item_subtotal = $item['sub_total'];


                   $item_vat = 0;


               $this->invoicer->addItem($item_product,'',$item_qty,$item_vat,$item_price,$item_discount,$item_subtotal);
               //}
          endforeach;
		  if(!empty($sales_acc)){
		   foreach($sales_acc as $acc):

               $acc_product = $acc['product_id'];
               $acc_qty = $acc['quantity'];
               $acc_price = $acc['price'];
               $acc_discount = '0';
               $acc_subtotal = $acc['sub_total'];


                   $acc_vat = 0;

               $this->invoicer->addSubItem($acc_product,'',$acc_qty,$acc_vat,$acc_price,$acc_discount,$acc_subtotal);
         
          endforeach;
		  }
           //Add totals
		    $this->invoicer->addTotal("Deposit",$sales_data['sale_deposit']);
			 $this->invoicer->addTotal("Deposit",$sales_data['sale_balance']);
           $this->invoicer->addTotal("Sub-Total",$sales_data['sale_subtotal']);

          // $this->invoicer->addTotal("Discount",'0');


               $this->invoicer->addTotal("Additional Accessories",$sales_data['sale_accessories_total']);


               $this->invoicer->addTotal("Delivery Fee",$sales_data['sale_transport']);

           $this->invoicer->addTotal("Total",$sales_data['sale_total'],true);
           //Add Badge
           $this->invoicer->addBadge('PAID');
           // Customer notes:

               $this->invoicer->addTitle("Notes");
               $this->invoicer->addParagraph('sasasasas');

           //Set Invoice Reference Notes
           $this->invoicer->setInfo('sasas');

           //Add Title
           $this->invoicer->addTitle("Payment information");
           //Add Paragraph
           $this->invoicer->addParagraph('Unikorn Technologies<br>Bank Name:KCB Moi Avenue Branch<br>Bank Code: N/A<br>Account Number: 01890929912');
           //Set footer note

           $this->invoicer->setFooternote('Order once placed cant be cancelled.Any such action will attract a percentage penalty fee on Order amount');
           $this->invoicer->setFooternoteLink('http://unikorn.co.ke/');
           //Render the PDF
           $this->invoicer->render('PO-'.$para2.'.pdf','D');
           /*$this->fpdf->SetFont('Arial','B',16);
           $this->fpdf->Cell(40,10,'Hello World!');

           echo $this->fpdf->Output('hello_world.pdf','D');*/

       elseif($para1=='invoice'):
           $data['sales_data'] = $this->crm_model->getSaleData('sales',$para2);
           $data['sales_items'] = $this->crm_model->getSaleItems('sale_items',$para2,'products');
           $data['sales_acc'] = $this->crm_model->getSaleItems('sale_items',$para2,'accessories');
           ini_set('memory_limit', '256M');
           // load library
           $this->load->library('pdf');
           $pdf = $this->pdf->load();
           $stylesheet = file_get_contents(FCPATH . '/public/css/bootstrap.min.css');
           $html = $this->load->view('sales/invoice', $data, true);
           $pdf->AddPage('P', // L - landscape, P - portrait
               '', '', '', '', 10, // margin_left
               10, // margin right
               10, // margin top
               10, // margin bottom
               5, // margin header
               10); // margin footer
           $pdf->WriteHTML($stylesheet, 1);
           $pdf->WriteHTML($html, 2);
           $output = 'PO-' .$para2 . '.pdf';
           $pdf->Output("$output", 'I');

       elseif($para1=='view'):
	   	 if (!$this->crm_model->check_permissions('sale','view')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
           if($this->input->post()):
               $this->crm_model->delete_row('sale_items','sale_id',$this->input->post('salesdb_id'));
               //Start Customer Edit customer Info
               $sale_customer=array(
                   "name" => $this->input->post('customer_name'),
                   "phone" => $this->input->post('phone'),
                   "address" => $this->input->post('address'),
               );
               $this->db->where('id', $this->input->post('customer_id'));

               $this->db->update('customers', $sale_customer);
   /**End Customer Edit**/

               if ($this->input->post('box')):
                   $cc=$this->input->post('box');
                   if (is_array($this->input->post('box'))):
                       $content='';
                       for ($i=0;$i<count($this->input->post('box'));$i++):
                           $content=$content."$cc[$i],";
                       endfor;
                       $accessories=$content;
                   endif;
               endif;
               $sale_data = array(
                   "customer_id" => $this->input->post('customer_id'),
                   "cashier_id" => $this->session->userdata('UserID'),
                   "uniqueID" => $this->input->post('sale_id'),
                   "sale_type" => $this->input->post('sale_type'),
                   "delivery_date" =>$this->input->post('delivery_date'),
                   "sale_deposit" => $this->input->post('pos_deposit'),
                   "sale_balance" =>$this->input->post('pos_balance'),
                   "sale_subtotal" =>$this->input->post('invoice_subtotal'),
                   "sale_accessories_total" =>$this->input->post('invoice_additional'),
                   "sale_transport" =>$this->input->post('invoice_shipping'),
                   "sale_total" =>$this->input->post('invoice_total'),
                   "sale_accessories" =>$accessories,
                   "status" =>$this->input->post('sale_status'),
                   "stage" =>$this->input->post('stage'),
                   "comments"=>$this->input->post('comments'),
                   "updated_at" => get_current_utc_time()
               );

               $this->db->where('id', $this->input->post('salesdb_id'));
               $this->db->update('sales', $sale_data);
              // $sales= $this->crm_model->saveDataID('sales',$sale_data);

               //Sale Items


               if ($this->input->post('pos_product')):

                   $products=$this->input->post('pos_product');
               foreach ($products as $key => $value):
                   $sale_items[] = array(
                       "sale_id"=>$this->input->post('salesdb_id'),
                       "product_id"=>$value,
                       "price"=>$this->input->post('pos_product_price')[$key],
                       "quantity"=>$this->input->post('pos_product_qty')[$key],
                       "sub_total"=>$this->input->post('pos_product_sub')[$key],
                       "item_type"=>'products',
                       "created_at" => get_current_utc_time()
                   );
               endforeach;

               if ($sale_items) {
                   $this->db->insert_batch('sale_items', $sale_items);
               }//E# if statement
               endif;


               if ($this->input->post('box')):

                   $accessories=$this->input->post('box');
                   if(in_array('comforters',$accessories))
                   {
                       $acc_itemsa=array(
                           "sale_id"=>$this->input->post('salesdb_id'),
                           "product_id"=>'comforters',
                           "price"=>$this->input->post('pos_price_comforters'),
                           "quantity"=>$this->input->post('pos_qty_comforters'),
                           "sub_total"=>$this->input->post('pos_subtotal_comforters'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsa);
                   }
                   if(in_array('scatters',$accessories))
                   {
                       $acc_itemsb=array(
                           "sale_id"=>$this->input->post('salesdb_id'),
                           "product_id"=>'scatters',
                           "price"=>$this->input->post('pos_price_scatters'),
                           "quantity"=>$this->input->post('pos_qty_scatters'),
                           "sub_total"=>$this->input->post('pos_subtotal_scatters'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsb);
                   }
                   if(in_array('footrest',$accessories))
                   {
                       $acc_itemsc=array(
                           "sale_id"=>$this->input->post('salesdb_id'),
                           "product_id"=>'footrest',
                           "price"=>$this->input->post('pos_price_footrest'),
                           "quantity"=>$this->input->post('pos_qty_footrest'),
                           "sub_total"=>$this->input->post('pos_subtotal_footrest'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsc);
                   }
                   if(in_array('others',$accessories))
                   {
                       $acc_itemsd=array(
                           "sale_id"=>$this->input->post('salesdb_id'),
                           "product_id"=>'others',
                           "price"=>$this->input->post('pos_price_others'),
                           "quantity"=>$this->input->post('pos_qty_others'),
                           "sub_total"=>$this->input->post('pos_subtotal_others'),
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                       $this->db->insert('sale_items', $acc_itemsd);
                   }
                   /*foreach ($accessories as $key=>$value):
                       $acc_items[]=array(
                           "sale_id"=>$this->input->post('salesdb_id'),
                           "product_id"=>$value,
                           "price"=>$this->input->post('pos_price_'.$value)[$key],
                           "quantity"=>$this->input->post('pos_qty_'.$value)[$key],
                           "sub_total"=>$this->input->post('pos_subtotal_'.$value)[$key],
                           "item_type"=>'accessories',
                           "created_at" => get_current_utc_time()
                       );
                   endforeach;

                   $this->db->insert_batch('sale_items', $acc_items);
*/
               endif;


               echo json_encode(array(
                   'status' => 'Success',
                   'message' => $this->input->post('sale_id')
               ));
           endif;

           $data['sales_data'] = $this->crm_model->getSaleData('sales',$para2);
           $data['sales_items'] = $this->crm_model->getSaleItems('sale_items',$para2,'products');
           $data['sales_acc'] = $this->crm_model->getSaleItems('sale_items',$para2,'accessories');
           $data['page_name']='sales/view';
           $this->load->view('index',$data);

       elseif($para1=='daily'):
           if($this->input->get('s_date')):
               $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d')=", $this->input->get('s_date'));
              // $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') <=",$this->input->get('e_date'));
           endif;
           if($this->input->get('sale_type') && !empty($this->input->get('sale_type'))):
               $this->db->where('sale_type', $this->input->get('sale_type'));
           endif;
           if($this->input->get('sale_status') && !empty($this->input->get('sale_status'))):
               $this->db->where('status', $this->input->get('sale_status'));
           endif;
           if($this->session->userdata('Role')!=1):
               $this->db->where('sale_branch_id', $this->session->userdata('Branch'));
           endif;
           $this->db->where('sale_type!=','New Sale');
           $data['sales']   = $this->db->get('sales')->result_array();
           $data['page_name']='sales/daily';
           $this->load->view('index',$data);
       else:
           if($this->input->get('s_date')):
               $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') >=", $this->input->get('s_date'));
               $this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') <=",$this->input->get('e_date'));
           endif;
           if($this->input->get('sale_type') && !empty($this->input->get('sale_type'))):
               $this->db->where('sale_type', $this->input->get('sale_type'));
           endif;
           if($this->input->get('sale_status') && !empty($this->input->get('sale_status'))):
               $this->db->where('status', $this->input->get('sale_status'));
           endif;
           if($this->session->userdata('Role')!=1):
               $this->db->where('sale_branch_id', $this->session->userdata('Branch'));
           endif;
           $data['sales']   = $this->db->get('sales')->result_array();
           $data['page_name']='sales/index';
           $this->load->view('index',$data);
       endif;

    }

    /**
     * @param string $para1
     * @param string $para2
     */
    public function customers($para1='', $para2=''){
        if($para1=='create'):
			 if (!$this->crm_model->check_permissions('customer','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
            if(!empty($this->input->post('name'))):

            if ($this->crm_model->is_email_exists('customers',$this->input->post('email'))) {
                echo json_encode(array("success" => false, 'message' =>'duplicate_email'));
                exit();
            }

            validate_submitted_data(array(
                "name" => "required",
                "phone" => "required"
            ));
            $customer_data = array(
                "email" => $this->input->post('email'),
                "name" => $this->input->post('name'),
                "phone" => $this->input->post('phone'),
                "address" => $this->input->post('address'),
                "address" => $this->input->post('address'),
                "created_at" => get_current_utc_time()
            );
            $customer= $this->crm_model->saveData('customers',$customer_data);
            if($customer_data):

            endif;
            endif;

            $data['page_name']='customers/create';
            $this->load->view('index',$data);

           elseif($para1=='edit'):
               if($this->input->post()):
                   $update_customer=array(
                       "name"=>$this->input->post('name'),
                       "email"=>$this->input->post('email'),
                       "phone"=>$this->input->post('phone'),
                       "address"=>$this->input->post('address'),
                       "updated_at" => get_current_utc_time()
                   );
                   $this->db->where('id', $this->input->post('customerID'));
                   $this->db->update('customers', $update_customer);
               endif;
               $data['customer']=$this->crm_model->getData('customers',$para2);
               $data['page_name']='customers/edit';
               $this->load->view('index',$data);

        else:
            $total= $this->db->get('customers')->num_rows();
            $data['customers']   = $this->db->get('customers')->result_array();
            $data['page_name']='customers/index';
            $this->load->view('index',$data);
        endif;


    }

    /**
     * @param string $para
     * @param string $para2
     */

    public function vendors($para1='',$para2='')
    {
        if($para1=='create'):
			 if (!$this->crm_model->check_permissions('vendor','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
            if(!empty($this->input->post('name'))):

                if ($this->crm_model->is_email_exists('suppliers',$this->input->post('email'))) {
                    echo json_encode(array("success" => false, 'message' =>'duplicate_email'));
                    exit();
                }

                validate_submitted_data(array(
                    "name" => "required",
                    "phone" => "required"
                ));
                $vendor_data = array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('name'),
                    "company_name" => $this->input->post('company_name'),
                    "phone" => $this->input->post('phone'),
                    "address" => $this->input->post('address'),
                    "address" => $this->input->post('address'),
                    "created_at" => get_current_utc_time()
                );
                $customer= $this->crm_model->saveData('suppliers',$vendor_data);
                if($vendor_data):

                endif;
            endif;

            $data['page_name']='vendors/create';
            $this->load->view('index',$data);
        elseif($para1=='edit'):
            if($this->input->post()):
                $update_vendor=array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('name'),
                    "company_name" => $this->input->post('company_name'),
                    "phone" => $this->input->post('phone'),
                    "address" => $this->input->post('address'),
                    "address" => $this->input->post('address'),
                    "updated_at" => get_current_utc_time()
                );
                $this->db->where('id', $this->input->post('vendorID'));
                $this->db->update('suppliers', $update_vendor);
            endif;
            $data['vendor']=$this->crm_model->getData('suppliers',$para2);
            $data['page_name']='vendors/edit';
            $this->load->view('index',$data);
        else:
            $total= $this->db->get('suppliers')->num_rows();
            $data['vendors']   = $this->db->get('suppliers')->result_array();
            $data['page_name']='vendors/index';
            $this->load->view('index',$data);
        endif;



    }

    /**
     * @param string $para1
     * @param string $para2
     */
    public function products($para1='', $para2='',$para3='',$para4=''){
        if($para1=='create'):
		 if (!$this->crm_model->check_permissions('product','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
            if(!empty($this->input->post('name'))):
                validate_submitted_data(array(
                    "name" => "required",

                ));
                $product_data = array(
                    "name" => $this->input->post('name'),
                    "min_price" => $this->input->post('min_price'),
                    "price" => $this->input->post('price'),
                    "category" => $this->input->post('category'),
                    "description" => $this->input->post('specs'),
                    "created_at" => get_current_utc_time()
                );
                $product_data= $this->crm_model->saveData('products',$product_data);
                if($product_data):

                endif;
            endif;

            $data['page_name']='products/create';
            $this->load->view('index',$data);

        elseif($para1=='edit'):
            if($this->input->post()):
                $update_product=array(
                    "name" => $this->input->post('name'),
                    "min_price" => $this->input->post('min_price'),
                    "price" => $this->input->post('price'),
                    "category" => $this->input->post('category'),
                    "description" => $this->input->post('specs'),
                    "updated_at" => get_current_utc_time()
                );
                $this->db->where('id', $this->input->post('productID'));
                $this->db->update('products', $update_product);
            endif;
            $data['product']=$this->crm_model->getData('products',$para2);
            $data['page_name']='products/edit';
            $this->load->view('index',$data);

        elseif($para1=='categories'):

           // $this->db->where('deleted_at',Null);


                    if($para2=='create'):
			                if (!$this->crm_model->check_permissions('category','create')):
						    redirect(base_url() . 'crm/errors/index');
                            endif;
					         if ($this->input->post()):
                                $category_data = array(
                                "name" => $this->input->post('name'),
                                "created_at" => get_current_utc_time());
                                $this->crm_model->saveData('categories',$category_data);
                                endif;
                              $data['page_name']='products/categories/create';
                              $this->load->view('index',$data);
                    elseif($para2=='edit'):
                        if (!$this->crm_model->check_permissions('category','create')):
                            redirect(base_url() . 'crm/errors/index');
                        endif;
                        if ($this->input->post()):
                            $category_data = array(
                                "name" => $this->input->post('name'),
                                "updated_at" => get_current_utc_time());
                            $this->db->where('id', $this->input->post('categoryID'));
                            $this->db->update('categories', $category_data);
                        endif;
                        $data['category']=$this->crm_model->getData('categories',$para3);
                        $data['page_name']='products/categories/edit';
                        $this->load->view('index',$data);
                    else:
                        $data['categories']   = $this->db->get('categories')->result_array();
                        $data['page_name']='products/categories/index';
                        $this->load->view('index',$data);
                    endif;



        else:
            $total= $this->db->get('products')->num_rows();
            $this->db->where('deleted_at',null);
            $data['products']   = $this->db->get('products')->result_array();
            $data['page_name']='products/index';
            $this->load->view('index',$data);
        endif;





    }

    public function users($para1='',$para2='')
    {
        if($para1=='create'):
		 if (!$this->crm_model->check_permissions('user','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
            if(!empty($this->input->post('name'))):

                if ($this->crm_model->is_email_exists('users',$this->input->post('email'))) {
                    echo json_encode(array("success" => false, 'message' =>'duplicate_email'));
                    exit();
                }

                validate_submitted_data(array(
                    "name" => "required",
                    "email" => "required"
                ));
                $user_data = array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('name'),
                    "role_id" => $this->input->post('role_id'),
					"branch" => $this->input->post('branch_id'),
                    "password" => sha1($this->input->post('password')),
                    "created_at" => get_current_utc_time()
                );
                $customer= $this->crm_model->saveData('users',$user_data);
                if($user_data):

                endif;
            endif;

            $data['page_name']='users/create';
            $this->load->view('index',$data);
        elseif($para1=='edit'):
            if($this->input->post()):
                $update_user=array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('name'),
                    "role_id" => $this->input->post('role_id'),
                    "branch" => $this->input->post('branch_id'),
                    "updated_at" => get_current_utc_time()
                );
                $this->db->where('id', $this->input->post('userID'));
                $this->db->update('users', $update_user);
            endif;
            $data['user']=$this->crm_model->getData('users',$para2);
            $data['page_name']='users/edit';
            $this->load->view('index',$data);
        else:
            $data['users']   = $this->db->get('users')->result_array();
            $data['page_name']='users/index';
            $this->load->view('index',$data);
        endif;
    }

    public function settings($para1='',$para2='',$para3='')
    {
        if ($para1=='profile'):
            if($this->input->post('name')):

            if($this->input->post('password')!=''):
                $user_data = array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('name'),
                    "role_id" => $this->input->post('role_id'),
                    "password" => sha1($this->input->post('password')),
                    "updated_at" => get_current_utc_time()
                );
            else:
            $user_data = array(
                "email" => $this->input->post('email'),
                "name" => $this->input->post('name'),
                "role_id" => $this->input->post('role_id'),
                "updated_at" => get_current_utc_time()
            );
            endif;
                $this->db->where('id', $this->input->post('userID'));
                $this->db->update('users', $user_data);
             endif;

            $userID=$this->session->userdata('UserID');
            $query=$this->crm_model->getDataById('users',$userID);
            $data['userData']=$query;
            $data['page_name']='settings/users/profile';
            $this->load->view('index',$data);

        elseif ($para1=='roles'):
					
			if($para2=='create'):
			 if (!$this->crm_model->check_permissions('role','create')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
				if($this->input->post()):
					$save_role=array(
					"name"=>$this->input->post('name'),
					"description"=>$this->input->post('description'),
					 "created_at" => get_current_utc_time()
					);
				$role= $this->crm_model->saveDataRID('roles',$save_role);
				$permissions=$this->input->post('permission_ids');
					foreach($permissions as $key=>$permit):
					  $save_permit[] = array(
                       "role_id"=>$role,
                       "permission_id"=>$permit,
                       "created_at" => get_current_utc_time());
					endforeach;
					$savePermit=$this->db->insert_batch('permission_role',$save_permit);
				endif;
			$data['page_name']='settings/roles/create';
			$this->load->view('index',$data);
			
			elseif($para2=='edit'):
			 if (!$this->crm_model->check_permissions('role','edit')):
						redirect(base_url() . 'crm/errors/index');
					 endif;
				if($this->input->post()):
					$update_role=array(
					"name"=>$this->input->post('name'),
					"description"=>$this->input->post('description'),
					 "updated_at" => get_current_utc_time()
					);
			   $this->db->where('id', $this->input->post('roleID'));
               $this->db->update('roles', $update_role);
			    $this->crm_model->delete_row('permission_role','role_id',$this->input->post('roleID'));
				$permissions=$this->input->post('permission_ids');
					foreach($permissions as $key=>$permit):
					  $save_permit[] = array(
                       "role_id"=>$this->input->post('roleID'),
                       "permission_id"=>$permit,
                       "created_at" => get_current_utc_time());
					endforeach;
					$savePermit=$this->db->insert_batch('permission_role',$save_permit);
				endif;
			$data['role_data']=$this->crm_model->getData('roles',$para3);
			$data['page_name']='settings/roles/edit';
			$this->load->view('index',$data);
			else:
                if (!$this->crm_model->check_permissions('role','index')):
                    redirect(base_url() . 'crm/errors/index');
                endif;
		$data['roles']   = $this->db->get('roles')->result_array();
		$data['page_name']='settings/roles/index';
		$this->load->view('index',$data);
				endif;
				 elseif($para1=='branch'):

           // $this->db->where('deleted_at',Null);


                    if($para2=='create'):
			                if (!$this->crm_model->check_permissions('branch','create')):
						    redirect(base_url() . 'crm/errors/index');
                            endif;
					         if ($this->input->post()):
                                $branch_data = array(
                                "name" => $this->input->post('name'),
                                "created_at" => get_current_utc_time());
                                $this->crm_model->saveData('branch',$branch_data);
                                endif;
                              $data['page_name']='settings/branch/create';
                              $this->load->view('index',$data);
                    elseif($para2=='edit'):
                        if (!$this->crm_model->check_permissions('branch','edit')):
                            redirect(base_url() . 'crm/errors/index');
                        endif;
                        if ($this->input->post()):
                            $branch_data = array(
                                "name" => $this->input->post('name'),
                                "updated_at" => get_current_utc_time());
                            $this->db->where('id', $this->input->post('branchID'));
                            $this->db->update('branch', $branch_data);
                        endif;
                        $data['branch']=$this->crm_model->getData('branch',$para3);
                        $data['page_name']='settings/branch/edit';
                        $this->load->view('index',$data);
                    else:
                        if (!$this->crm_model->check_permissions('branch','index')):
                            redirect(base_url() . 'crm/errors/index');
                        endif;
                        $data['branch']   = $this->db->get('branch')->result_array();
                        $data['page_name']='settings/branch/index';
                        $this->load->view('index',$data);
                    endif;

        elseif ($para1=='permissions'):

        endif;
    }
	public function errors()
	{
		$data['page_name']='errors/index';
		$this->load->view('index',$data);
	}

	public function common($para1='')
    {
        if($para1=='delete')
        {
            if ($this->input->post()):
            $this->crm_model->delete_row($this->input->post('db'),'id',$this->input->post('rowID'));
            redirect(base_url() . 'crm/'.$this->input->post('action'));
            endif;

        }
    }

	
	
	
	

}