-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2017 at 08:58 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sofazetu`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjustments`
--

CREATE TABLE `adjustments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `adjustment_items`
--

CREATE TABLE `adjustment_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `adjustment_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `adjustment` int(11) NOT NULL,
  `diff` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) DEFAULT '0',
  `created_at` varchar(50) DEFAULT '0',
  `updated_at` varchar(50) DEFAULT '0',
  `deleted_at` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sofas', '2017-04-27 21:00:42', '2017-04-27 21:00:42', 'Null');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sammy Kilonzi', NULL, '0711646779', 'Nairobi', NULL, NULL, NULL),
(2, 'Peter Makau', NULL, '0729547704', 'Mwingi', NULL, NULL, NULL),
(3, 'Carol Marugi', NULL, '0729547704', 'Nakuru', '2017-05-08 15:39:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_trackings`
--

CREATE TABLE `inventory_trackings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `trackable_id` int(11) NOT NULL,
  `trackable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventory_trackings`
--

INSERT INTO `inventory_trackings` (`id`, `user_id`, `product_id`, `trackable_id`, `trackable_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'App\\SaleItem', '2017-04-27 04:36:36', '2017-04-27 04:36:36'),
(2, 1, 2, 2, 'App\\SaleItem', '2017-04-27 04:36:36', '2017-04-27 04:36:36'),
(3, 1, 3, 3, 'App\\SaleItem', '2017-04-27 04:36:36', '2017-04-27 04:36:36'),
(4, 1, 5, 4, 'App\\SaleItem', '2017-04-27 04:36:36', '2017-04-27 04:36:36'),
(5, 1, 5, 5, 'App\\SaleItem', '2017-04-27 04:46:02', '2017-04-27 04:46:02'),
(6, 1, 6, 6, 'App\\SaleItem', '2017-04-27 04:46:02', '2017-04-27 04:46:02'),
(7, 1, 9, 7, 'App\\SaleItem', '2017-04-27 04:46:02', '2017-04-27 04:46:02'),
(8, 1, 8, 8, 'App\\SaleItem', '2017-04-27 04:46:02', '2017-04-27 04:46:02'),
(9, 2, 3, 9, 'App\\SaleItem', '2017-04-27 05:12:05', '2017-04-27 05:12:05'),
(10, 2, 2, 10, 'App\\SaleItem', '2017-04-27 05:12:05', '2017-04-27 05:12:05'),
(11, 2, 2, 11, 'App\\SaleItem', '2017-04-27 05:12:52', '2017-04-27 05:12:52'),
(12, 2, 5, 12, 'App\\SaleItem', '2017-04-27 05:12:52', '2017-04-27 05:12:52'),
(13, 2, 9, 13, 'App\\SaleItem', '2017-04-27 05:12:52', '2017-04-27 05:12:52'),
(14, 2, 11, 14, 'App\\SaleItem', '2017-04-27 05:12:52', '2017-04-27 05:12:52'),
(15, 1, 2, 15, 'App\\SaleItem', '2017-04-27 11:29:10', '2017-04-27 11:29:10'),
(16, 1, 3, 16, 'App\\SaleItem', '2017-04-27 11:29:10', '2017-04-27 11:29:10'),
(17, 1, 5, 17, 'App\\SaleItem', '2017-04-27 11:29:10', '2017-04-27 11:29:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_01_000001_create_oauth_auth_codes_table', 1),
('2016_06_01_000002_create_oauth_access_tokens_table', 1),
('2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
('2016_06_01_000004_create_oauth_clients_table', 1),
('2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
('2016_08_28_025021_create_table_products', 1),
('2016_08_28_093410_create_customers_table', 1),
('2016_08_28_121342_create_suppliers_table', 1),
('2016_08_28_153818_create_roles_table', 1),
('2016_08_28_162812_create_permissions_table', 1),
('2016_08_28_164357_create_table_permission_role', 1),
('2016_08_28_165002_alter_table_users_add_role', 1),
('2016_09_05_155020_create_sales_table', 1),
('2016_09_28_152136_create_sales_items_table', 1),
('2016_10_19_093059_create_receivings_table', 1),
('2016_10_19_144732_create_receiving_items_table', 1),
('2016_10_20_140622_create_adjustments_table', 1),
('2016_10_20_140810_create_adjustment_items_table', 1),
('2016_10_20_151126_alter_table_products_add_quantity', 1),
('2016_10_22_012331_create_inventory_trackings_table', 1),
('2016_10_22_054824_alter_table_sales_add_comments', 1),
('2016_10_30_034307_create_settings_table', 1),
('2016_12_27_083228_alter_product_price_value', 1),
('2017_01_24_172235_alter_master_table_add_softdelete_field', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('aa5bbb7c2e4e64f071ba231ad7587fe2bec47ba186e3e4e7230f5e10df128cc0b724db2fd2d74ef4', 2, 1, 'Default', '[]', 0, '2017-04-27 04:53:40', '2017-04-27 04:53:40', '2117-04-27 07:53:40'),
('e03842039365fa2e154718de83238eaf2a7d93a0746a0f5a6ee7fa3debf056a336afcadd6b339c9f', 1, 1, 'Default', '[]', 0, '2017-04-27 04:19:44', '2017-04-27 04:19:44', '2117-04-27 07:19:44');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'My Application Personal Access Client', '3la4SHl0pORn8WeR4nYqH51Qwo6PZBGHzaIWcYnz', 'http://localhost', 1, 0, 0, '2017-04-27 04:19:24', '2017-04-27 04:19:24'),
(2, NULL, 'My Application Password Grant Client', 'bB51YWWM1gHWiQYu6SfsRsHgCHCKuD9JLiOdlIp4', 'http://localhost', 0, 1, 0, '2017-04-27 04:19:25', '2017-04-27 04:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-04-27 04:19:24', '2017-04-27 04:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `object` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `object`, `action`, `created_at`, `updated_at`) VALUES
(1, 'accesstoken', 'issuetoken', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(2, 'transienttoken', 'refresh', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(3, 'client', 'foruser', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(4, 'client', 'destroy', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(5, 'scope', 'all', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(6, 'personalaccesstoken', 'foruser', '2017-04-27 04:19:38', '2017-04-27 04:19:38'),
(7, 'personalaccesstoken', 'destroy', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(8, 'home', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(9, 'customer', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(10, 'customer', 'create', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(11, 'customer', 'show', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(12, 'customer', 'edit', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(13, 'customer', 'destroy', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(14, 'vendor', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(15, 'vendor', 'create', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(16, 'vendor', 'show', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(17, 'vendor', 'edit', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(18, 'vendor', 'destroy', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(19, 'product', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(20, 'product', 'create', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(21, 'product', 'show', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(22, 'product', 'edit', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(23, 'product', 'destroy', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(24, 'user', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(25, 'user', 'create', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(26, 'user', 'show', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(27, 'user', 'edit', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(28, 'user', 'destroy', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(29, 'sale', 'create', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(30, 'sale', 'receipt', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(31, 'receiving', 'index', '2017-04-27 04:19:39', '2017-04-27 04:19:39'),
(32, 'receiving', 'create', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(33, 'receiving', 'show', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(34, 'adjustment', 'index', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(35, 'adjustment', 'create', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(36, 'adjustment', 'show', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(37, 'tracking', 'index', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(38, 'report', 'index', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(39, 'report', 'show', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(40, 'profile', 'edit', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(41, 'setting', 'edit', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(42, 'role', 'index', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(43, 'role', 'create', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(44, 'role', 'show', '2017-04-27 04:19:40', '2017-04-27 04:19:40'),
(45, 'role', 'edit', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(46, 'role', 'destroy', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(47, 'permission', 'index', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(48, 'permission', 'create', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(49, 'permission', 'show', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(50, 'permission', 'edit', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(51, 'permission', 'destroy', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(52, 'api\\customer', 'index', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(53, 'api\\product', 'index', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(54, 'api\\supplier', 'index', '2017-04-27 04:19:41', '2017-04-27 04:19:41'),
(55, 'sale', 'index', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(5, 2, 9, '2017-05-06 15:58:13', NULL),
(6, 2, 10, '2017-05-06 15:58:13', NULL),
(7, 2, 40, '2017-05-06 15:58:13', NULL),
(8, 2, 29, '2017-05-06 15:58:13', NULL),
(9, 2, 30, '2017-05-06 15:58:13', NULL),
(10, 2, 55, '2017-05-06 15:58:13', NULL),
(11, 2, 14, '2017-05-06 15:58:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(12,2) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `min_price` decimal(12,2) DEFAULT NULL,
  `category` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `barcode`, `name`, `description`, `price`, `quantity`, `created_at`, `updated_at`, `deleted_at`, `min_price`, `category`) VALUES
(1, '54899', 'Nutela', 'Molestiae quo id et. Nihil accusamus eum at qui eum sunt qui. Cupiditate est earum vero tempore dolorem.', '306.00', -1, '2017-04-27 04:19:59', '2017-04-27 15:35:25', '2017-04-27 15:35:25', NULL, NULL),
(2, '856', 'Nu green tea', 'Nisi autem nulla aliquid. Dolores possimus quia placeat commodi ab qui consequatur.', '26.00', -4, '2017-04-27 04:19:59', '2017-04-27 15:35:26', '2017-04-27 15:35:26', NULL, NULL),
(3, '458548', 'UC1000', 'Similique ex nihil ipsa officiis amet. Est est amet odit in est. Laudantium a repudiandae vel quae quia beatae. Inventore molestias voluptatibus optio autem sit maxime numquam.', '394.00', -3, '2017-04-27 04:19:59', '2017-04-27 15:35:28', '2017-04-27 15:35:28', NULL, NULL),
(4, '5536623', 'Taro', 'Delectus praesentium illum est et. Velit dolorem voluptatibus blanditiis velit inventore et neque. Et itaque ipsam unde cupiditate. Et assumenda vitae animi iste omnis aperiam harum.', '632.00', 0, '2017-04-27 04:19:59', '2017-04-27 15:35:29', '2017-04-27 15:35:29', NULL, NULL),
(5, '823', 'Cheetoz', 'Repudiandae dicta labore earum quidem velit expedita et omnis. Iusto qui et sapiente voluptates labore qui quos.', '518.00', -4, '2017-04-27 04:19:59', '2017-04-27 15:35:31', '2017-04-27 15:35:31', NULL, NULL),
(6, '4730', 'Peejoy', 'Rerum et est doloribus laboriosam. Qui consequatur error sit maxime. Nobis quae soluta alias consequatur rerum ratione quia. Dicta similique saepe qui repellat natus sit cum.', '172.00', -1, '2017-04-27 04:19:59', '2017-04-27 15:35:34', '2017-04-27 15:35:34', NULL, NULL),
(7, '950210', 'Nutrisari', 'Maiores aut ea repellendus veniam. Non aspernatur esse laboriosam consequatur illum. Dolorum et sunt accusamus blanditiis maxime repellat.', '170.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:35', '2017-04-27 15:35:35', NULL, NULL),
(8, '238697', 'Chimori', 'Repudiandae sit cupiditate placeat. Esse aliquid eum provident. Laborum sit reprehenderit est praesentium harum recusandae sit. Cupiditate est cumque quos odit ad et quo.', '894.00', -1, '2017-04-27 04:20:00', '2017-04-27 15:35:37', '2017-04-27 15:35:37', NULL, NULL),
(9, '50812', 'Milkita', 'Recusandae perferendis inventore quasi impedit sed vitae autem. Sed officia et nesciunt ut architecto nihil. Non odit et non sunt. Voluptates nesciunt voluptas architecto corrupti.', '626.00', -2, '2017-04-27 04:20:00', '2017-04-27 15:35:38', '2017-04-27 15:35:38', NULL, NULL),
(10, '53544', 'Chitato', 'Ratione ipsum sunt quas consequuntur placeat est. Minima odit consequuntur omnis ratione. Et quis sunt officiis omnis quia consequatur atque. Dolorem earum magni dicta aut.', '421.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:40', '2017-04-27 15:35:40', NULL, NULL),
(11, '70', 'Malkis', 'Rerum id nesciunt minima. Quae nesciunt deserunt fugiat aut aperiam. Nemo ut porro veritatis.', '599.00', -1, '2017-04-27 04:20:00', '2017-04-27 15:35:41', '2017-04-27 15:35:41', NULL, NULL),
(12, '50', 'Fanta', 'Est quaerat asperiores quas optio voluptatum. Ea omnis beatae dolor sapiente totam quas molestiae. Quia nam dolore qui molestiae vitae aut.', '598.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:42', '2017-04-27 15:35:42', NULL, NULL),
(13, '218', 'Coca cola', 'Quod dolor qui numquam temporibus. Voluptas eos blanditiis iste rerum adipisci omnis. Ipsam eius amet necessitatibus iste.', '812.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:44', '2017-04-27 15:35:44', NULL, NULL),
(14, '4', 'Sprite', 'In fugit sint debitis iusto ut sit voluptatem et. Nulla voluptatem molestias laboriosam veniam consectetur voluptatem quos. Labore consequatur nostrum quae iusto perspiciatis.', '981.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:45', '2017-04-27 15:35:45', NULL, NULL),
(15, '552500226', 'Silverqueen', 'Possimus quasi sed id qui. Aliquam omnis fugiat sunt nobis consequatur amet. Nobis qui dolor voluptatum aut. Maiores dignissimos qui aut porro sint odio.', '255.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:47', '2017-04-27 15:35:47', NULL, NULL),
(16, '3240049', 'Indomie goreng', 'Ipsa inventore deserunt soluta repellendus. Recusandae ut ipsa natus esse et et. Nisi dicta perspiciatis consequatur fuga iusto sed fugiat. Dolorem non quaerat iste quas.', '355.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:48', '2017-04-27 15:35:48', NULL, NULL),
(17, '4014106', 'Indomie ayam bawang', 'Rem aut impedit suscipit animi quis non maxime. Aut magni est assumenda perferendis.', '711.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:50', '2017-04-27 15:35:50', NULL, NULL),
(18, '70371915', 'Teh Tarik', 'Tempora tenetur nulla et. Et tempora eum recusandae et deleniti est. Voluptas a ut accusantium doloribus ut quae quos. Ut eos ratione iure quas aut deserunt quia.', '695.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:51', '2017-04-27 15:35:51', NULL, NULL),
(19, '71357', 'Kopi kapal api', 'Dolore temporibus veritatis dolor est nesciunt aut. Enim sint quod et dolorum sed ipsa. Provident fugiat praesentium voluptas quis. Ut exercitationem nostrum eum sapiente quo voluptatem.', '560.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:53', '2017-04-27 15:35:53', NULL, NULL),
(20, '7422', 'God day coffee', 'Provident voluptas modi aperiam quod magnam sapiente et reiciendis. Quasi expedita libero velit cum. Perferendis voluptates sint tenetur.', '98.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:54', '2017-04-27 15:35:54', NULL, NULL),
(21, '681649', 'Kacang dua kelinci', 'Ipsum quia veniam consequatur ut ut. Dolor occaecati commodi sed praesentium. Aperiam quo veritatis porro consequuntur.', '515.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:56', '2017-04-27 15:35:56', NULL, NULL),
(22, '8', 'Kacang Garuda', 'Aut id harum autem consectetur sapiente sapiente. Omnis accusamus dolore ad ea excepturi eum voluptatum. Aut id non qui asperiores voluptatum.', '852.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:57', '2017-04-27 15:35:57', NULL, NULL),
(23, '2229', 'Gery salut', 'Harum eos aut temporibus ab aut nihil. Et dolor animi autem quam ut. A praesentium architecto tempora doloribus.', '868.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:35:59', '2017-04-27 15:35:59', NULL, NULL),
(24, '9647', 'Mizone', 'Sed harum aut voluptatem. Minima dolorum assumenda laboriosam dolor soluta ad. Aut aut quasi quidem est sed.', '503.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:36:00', '2017-04-27 15:36:00', NULL, NULL),
(25, '126692678', 'Aqua', 'Cupiditate ut enim nihil sed voluptatem. Nemo suscipit iure velit est et sint placeat nulla. Expedita et odit ab nesciunt et. Ea facilis qui praesentium officia.', '57.00', 0, '2017-04-27 04:20:00', '2017-04-27 15:36:02', '2017-04-27 15:36:02', NULL, NULL),
(26, '44602', 'Kopiko', 'Sint voluptatem non porro sed tempore. Nobis eaque dolorem in veritatis est. Sed maiores omnis qui inventore libero voluptate neque. A eos omnis molestiae sit aliquid nobis.', '98.00', 0, '2017-04-27 04:20:01', '2017-04-27 15:36:04', '2017-04-27 15:36:04', NULL, NULL),
(27, '656683', 'Pop mie', 'Molestiae sint eum molestiae cum. Molestiae ut laudantium et repellat. Est dolores optio quas qui. Earum minima blanditiis nulla accusantium et.', '760.00', 0, '2017-04-27 04:20:01', '2017-04-27 15:35:22', '2017-04-27 15:35:22', NULL, NULL),
(28, NULL, 'L-Shape 7 Seater Sofa', NULL, '85000.00', 0, '2017-04-27 15:35:14', '2017-04-27 15:35:14', NULL, NULL, NULL),
(29, NULL, '2 Seater Sofa', NULL, '15000.00', 0, '2017-04-27 15:36:20', '2017-04-27 15:36:20', NULL, NULL, NULL),
(30, NULL, 'Corner-Shape 6 Seater Sofa', NULL, '65000.00', 0, '2017-04-27 15:49:25', '2017-04-27 15:49:25', NULL, '55000.00', 'Sofas'),
(31, NULL, 'Cream Cushions', 'Test Please', '1500.00', 0, '2017-04-30 17:28:58', NULL, NULL, '1200.00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `receivings`
--

CREATE TABLE `receivings` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `receiving_items`
--

CREATE TABLE `receiving_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `receiving_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Role for administrator', '2017-04-27 04:19:42', '2017-04-27 04:19:42'),
(2, 'Users', 'System Users', '2017-04-27 04:52:40', '2017-05-06 15:58:13'),
(3, 'Partners', 'Testing for role insert', '2017-05-06 05:03:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `uniqueID` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `cashier_id` int(11) NOT NULL,
  `sale_type` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_date` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_deposit` decimal(10,2) DEFAULT NULL,
  `sale_balance` decimal(10,2) DEFAULT NULL,
  `sale_subtotal` decimal(10,2) DEFAULT NULL,
  `sale_accessories_total` decimal(10,2) DEFAULT NULL,
  `sale_transport` decimal(10,2) DEFAULT NULL,
  `sale_total` decimal(10,2) DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'In Progress',
  `stage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_accessories` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `uniqueID`, `customer_id`, `cashier_id`, `sale_type`, `delivery_date`, `sale_deposit`, `sale_balance`, `sale_subtotal`, `sale_accessories_total`, `sale_transport`, `sale_total`, `comments`, `status`, `stage`, `sale_accessories`, `created_at`, `updated_at`) VALUES
(1, '100', 1, 1, 'Order', '2017-05-17', '30000.00', '35000.00', '65000.00', '600.00', '2500.00', '68100.00', 'test please      ', 'Completed', '', 'comforters,', '2017-05-03 10:40:17', '2017-05-03 12:11:41'),
(2, '101', 2, 1, 'Order', '2017-05-13', '20000.00', '25000.00', '45000.00', '16600.00', '2000.00', '63600.00', NULL, 'In Progress', NULL, 'comforters,others,', '2017-05-03 12:08:56', NULL),
(3, '102', 2, 1, 'Order', '2017-05-13', '25000.00', '5000.00', '30000.00', '6000.00', '2500.00', '38500.00', NULL, 'In Progress', NULL, 'comforters,', '2017-05-03 13:02:50', NULL),
(4, '103', 3, 1, 'Order', '2017-05-08', '20000.00', '25000.00', '45000.00', '5900.00', '2000.00', '52900.00', NULL, 'In Progress', NULL, 'comforters,footrest,', '2017-05-08 15:39:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `item_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`id`, `sale_id`, `product_id`, `price`, `quantity`, `sub_total`, `item_type`, `created_at`, `updated_at`) VALUES
(3, 2, 'Corner-Shape 6 Seater Sofa', '45000.00', 1, '45000.00', 'products', '2017-05-03 12:08:56', NULL),
(4, 2, 'comforters', '2500.00', 6, '15000.00', 'accessories', '2017-05-03 12:08:56', NULL),
(5, 2, 'others', '800.00', 2, '1600.00', 'accessories', '2017-05-03 12:08:56', NULL),
(6, 1, 'Corner-Shape 6 Seater Sofa', '65000.00', 1, '65000.00', 'products', '2017-05-03 12:11:42', NULL),
(7, 1, 'comforters', '300.00', 2, '600.00', 'accessories', '2017-05-03 12:11:42', NULL),
(8, 3, 'Cream Cushions', '15000.00', 2, '30000.00', 'products', '2017-05-03 13:02:50', NULL),
(9, 3, 'comforters', '3000.00', 2, '6000.00', 'accessories', '2017-05-03 13:02:50', NULL),
(10, 4, '6 seater corner seat', '45000.00', 1, '45000.00', 'products', '2017-05-08 15:39:12', NULL),
(11, 4, 'comforters', '400.00', 6, '2400.00', 'accessories', '2017-05-08 15:39:12', NULL),
(12, 4, 'footrest', '3500.00', 1, '3500.00', 'accessories', '2017-05-08 15:39:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `label`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_per_page', 'Data Perpage', '20', '2017-04-27 04:19:46', '2017-04-27 04:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in` int(10) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `logged_in`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Sofazetu', 'sales@sofazetu.co.ke', 'd699ed7eda39f1c61721cdf53ee4753572831cd1', 1, 'UydocXxWW8EVNvjI7GQRGDba3bovYyq8MUquzL6KizZJUdZpxifmrdpLrf0L', 1, '2017-05-08 15:49:08', '2017-04-27 04:19:43', '2017-05-08 10:37:00'),
(2, 'Uchumi Kenya', 'uchumike@mail.com', '$2y$10$p3LCdQIg./fN/L012fw0ou4jQuNpE4a//R5bq0yhek.EVebj5bJJW', 2, 'ob7KjboXnA9FZ2q2u6bDOoL6sKGV5Z88oLwcNi8nNFqq4v4G17cpBJQ7am2J', NULL, NULL, '2017-04-27 04:53:37', '2017-04-27 05:21:44'),
(3, 'Kilonzi Sammy', 'klozjeff@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, NULL, 0, '2017-05-08 15:49:03', '2017-05-06 07:04:11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjustments`
--
ALTER TABLE `adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adjustment_items`
--
ALTER TABLE `adjustment_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `inventory_trackings`
--
ALTER TABLE `inventory_trackings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receivings`
--
ALTER TABLE `receivings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receiving_items`
--
ALTER TABLE `receiving_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `suppliers_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjustments`
--
ALTER TABLE `adjustments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `adjustment_items`
--
ALTER TABLE `adjustment_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inventory_trackings`
--
ALTER TABLE `inventory_trackings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `receivings`
--
ALTER TABLE `receivings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receiving_items`
--
ALTER TABLE `receiving_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
