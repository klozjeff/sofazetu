
if (typeof jQuery === 'undefined') {
    throw new Error('POS JavaScript requires jQuery')
}

$(document).ready(function() {
var app_url='http://localhost/sofazetu/';
    // Sale Type
    $('#sale_type').change(function() {
        var saleType = $("#sale_type option:selected").text();
        $(".sale_type").text(saleType);

        if(saleType=='Ready Made')
        {
           $("#pos_deposit").attr('disabled',true);
            document.getElementById("pos_balance").val='0.00';
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

            $('.delivery_date').val(today);

        }
        else
        {
            $("#pos_deposit").removeAttr('disabled');
            var now = new Date();
            var numberOfDaysToAdd = 10;
            now.setDate(now.getDate() + numberOfDaysToAdd);
            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day) ;


            $('.delivery_date').val(today);
        }

    });
    $('#sale_status').change(function() {
        var stageType = $("#sale_status option:selected").val();
        $(".sale_status").text(stageType);

        if(stageType=='In Progress')
        {
            $("#stages").show();

        }
        else
        {
            $("#stages").hide();

        }

    });



    // Load dataTables
    $("#data-table").dataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                class:'btn',
                exportOptions: {
                    columns: ':contains("Office")'
                }

            },
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    // download CSV
    $(document).on('click', ".download-csv", function(e) {
        e.preventDefault;

        var action = 'action=download_csv'; //build a post data structure
        downloadCSV(action);

    });

    $(document).on('click', ".item-select", function(e) {

        e.preventDefault;
        var product = $(this);
        $('#insert').modal({ backdrop: 'static', keyboard: false }).one('click', '#selected', function(e) {

           var itemText = $('#insert').find("option:selected").text();
            var itemValue = $('#insert').find("option:selected").val();

            $(product).closest('tr').find('.invoice_product').val(itemText);
            $(product).closest('tr').find('.invoice_product_price').val(itemValue);

            updateTotals('.calculate');
            calculateTotal();


        });

    return false;

    });


    // enable date pickers for due date and invoice date
    var dateFormat = $(this).attr('data-vat-rate');
    $('#sale_delivery_date, #s_date,#e_date').datetimepicker({
        showClose: false,
        format: dateFormat
    });
    $("#box1").on("change", function () {
        $(".access_qty_1,.access_price_1").prop("disabled", !$(this).is(":checked"));
    });
    $("#box2").on("change", function () {
        $(".access_qty_2,.access_price_2").prop("disabled", !$(this).is(":checked"));
    });
    $("#box3").on("change", function () {
        $(".access_qty_3,.access_price_3").prop("disabled", !$(this).is(":checked"));
    });
    $("#box4").on("change", function () {
        $(".access_qty_4,.access_price_4").prop("disabled", !$(this).is(":checked"));
    });

    // create sale
    $("#deleteRow").click(function(e) {

        e.preventDefault();
        actionDeleteRow(1.2);
    });
    // create sale
    $("#action_create_sale").click(function(e) {

        e.preventDefault();
        actionCreateSale();
    });
    // create edit
    $("#action_edit_sale").click(function(e) {

        e.preventDefault();
        actionEditSale();
    });

    // copy customer details to shipping
    $('input.copy-input').on("input", function () {
        $('input#' + this.id + "_ship").val($(this).val());
    });

    // remove product row
    $('#invoice_table').on('click', ".delete-row", function(e) {
        e.preventDefault();
        $(this).closest('tr').remove();
        calculateTotal();
    });

    // add new product row on invoice
    var cloned = $('#invoice_table tr:last').clone();
    $(".add-row").click(function(e) {
        e.preventDefault();
        cloned.clone().appendTo('#invoice_table');
    });

   // calculateTotal();

    $('#invoice_table').on('input', '.calculate', function () {
        updateTotals(this);
        calculateTotal();
    });

    $('#invoice_totals').on('input', '.calculate', function () {
        calculateTotal();
    });

    $('#invoice_product').on('input', '.calculate', function () {
        calculateTotal();
    });

    $('.remove_vat').on('change', function() {
        calculateTotal();
    });

    $('#invoice_totals').on('input', '.calculate', function () {
        calculateTotal();
    });
/**Accessories 1**/
    $('#access_price_1').on("input", function() {
        $('.invoice_access_price1').val(updateAccessories1().toFixed(2));
    });

function updateAccessories1() {
    var qty = parseInt($("#access_qty_1").val()) || 0;
    var price = parseInt($("#access_price_1").val()) || 0;

    return qty * price;
}
/** End 1**/

    /**Accessories 2**/
    $('#access_price_2').on("input", function() {
        $('.invoice_access_price2').val(updateAccessories2().toFixed(2));
    });

    function updateAccessories2() {
        var qty = parseInt($("#access_qty_2").val()) || 0;
        var price = parseInt($("#access_price_2").val()) || 0;

        return qty * price;
    }
    /** End 2**/

    /**Accessories 3**/
    $('#access_price_3').on("input", function() {
        $('.invoice_access_price3').val(updateAccessories3().toFixed(2));
    });

    function updateAccessories3() {
        var qty = parseInt($("#access_qty_3").val()) || 0;
        var price = parseInt($("#access_price_3").val()) || 0;

        return qty * price;
    }
    /** End 3**/

    /**Accessories 4**/
    $('#access_price_4').on("input", function() {
        $('.invoice_access_price4').val(updateAccessories4().toFixed(2));
    });

    function updateAccessories4() {
        var qty = parseInt($("#access_qty_4").val()) || 0;
        var price = parseInt($("#access_price_4").val()) || 0;

        return qty * price;
    }
    /** End 4**/

    function updateTotals(elem) {

        var tr = $(elem).closest('tr'),
            quantity = $('[name="pos_product_qty[]"]', tr).val(),
            price = $('[name="pos_product_price[]"]', tr).val(),
            subtotal = parseInt(quantity) * parseFloat(price);
        $('.calculate-sub', tr).val(subtotal.toFixed(2));
    }
    $('#accessories').on('input', '.calculateaccess', function () {
        calculateAdditional();
        calculateTotal();

    });


    $('#pay').on('input', '.deposit', function () {
        getBalance();
        calculateTotal();
    });
    $('#pay').on('input', '.calculate.shipping', function () {
        calculateTotal();
    });

    function calculateAdditional()
    {
       /* parseInt($(".invoice_access_price4").val()) + parseInt($(".invoice_access_price3").val())  +
        parseInt($(".invoice_access_price2").val())+*/
       var op1=$('.invoice_access_price1').val()|| 0;
        var op2=$('.invoice_access_price2').val()|| 0;
        var op3=$('.invoice_access_price3').val()|| 0;
        var op4=$('.invoice_access_price4').val()|| 0;
        var additional=parseFloat(op1)+parseFloat(op2)+parseFloat(op3)+parseFloat(op4);
        $('.invoice-additional').text(additional.toFixed(2));
        $('#invoice_additional').val(additional.toFixed(2));


    }

    function getBalance() {
        var grandTotal = 0,
            disc = 0;
        $('#invoice_table tbody tr').each(function() {
            var c_sbt = $('.calculate-sub', this).val(),
                quantity = $('[name="pos_product_qty[]"]', this).val(),
                price = $('[name="pos_product_price[]"]', this).val() || 0,
                subtotal = parseInt(quantity) * parseFloat(price);

            grandTotal += parseFloat(c_sbt);
            disc += subtotal - parseFloat(c_sbt);
        });
        var subT = parseFloat(grandTotal);

        var depositBal=parseInt(subT)-parseInt($('.deposit').val()) || 0;

        $('.pos-balance').text(depositBal.toFixed(2));
        $('#pos_balance').val(depositBal.toFixed(2));
    }
    function calculateTotal() {

        var grandTotal = 0,
            disc = 0,
            c_ship = parseInt($('.calculate.shipping').val()) || 0,
            a_cost = parseInt($('#invoice_additional').val()) || 0;


        $('#invoice_table tbody tr').each(function() {
            var c_sbt = $('.calculate-sub', this).val(),
                quantity = $('[name="pos_product_qty[]"]', this).val(),
                price = $('[name="pos_product_price[]"]', this).val() || 0,
                subtotal = parseInt(quantity) * parseFloat(price);

            grandTotal += parseFloat(c_sbt);
            disc += subtotal - parseFloat(c_sbt);
        });




        // VAT, DISCOUNT, SHIPPING, TOTAL, SUBTOTAL:
        var subT = parseFloat(grandTotal),
            finalTotal = parseFloat(grandTotal + c_ship + a_cost),
            vat = parseInt($('.invoice-vat').attr('data-vat-rate'));


        $('.invoice-sub-total').text(subT.toFixed(2));
        $('#invoice_subtotal').val(subT.toFixed(2));
        $('.invoice-discount').text(disc.toFixed(2));
        $('#invoice_discount').val(disc.toFixed(2));

        if($('.invoice-vat').attr('data-enable-vat') === '1') {

            if($('.invoice-vat').attr('data-vat-method') === '1') {
                $('.invoice-vat').text(((vat / 100) * finalTotal).toFixed(2));
                $('#invoice_vat').val(((vat / 100) * finalTotal).toFixed(2));
                $('.invoice-total').text((finalTotal).toFixed(2));
                $('#invoice_total').val((finalTotal).toFixed(2));
            } else {
                $('.invoice-vat').text(((vat / 100) * finalTotal).toFixed(2));
                $('#invoice_vat').val(((vat / 100) * finalTotal).toFixed(2));
                $('.invoice-total').text((finalTotal + ((vat / 100) * finalTotal)).toFixed(2));
                $('#invoice_total').val((finalTotal + ((vat / 100) * finalTotal)).toFixed(2));
            }
        } else {
            $('.invoice-total').text((finalTotal).toFixed(2));
            $('#invoice_total').val((finalTotal).toFixed(2));
        }

        // remove vat
        if($('input.remove_vat').is(':checked')) {
            $('.invoice-vat').text("0.00");
            $('#invoice_vat').val("0.00");
            $('.invoice-total').text((finalTotal).toFixed(2));
            $('#invoice_total').val((finalTotal).toFixed(2));
        }

    }


    function actionCreateSale(){

      var errorCounter = validateForm();

        if (errorCounter > 0) {
            $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#response .message").html("<strong>Error</strong>: It appear's you have forgotten to complete something!");
            $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);

        } else {

            var $btn = $("#action_create_sale").button("loading");
            $(".required").parent().removeClass("has-error");
            $("#create_sale").find(':input:disabled').removeAttr('disabled');
            var formData = ($('#create_sale').serialize());
            var postData = formData;
            //alert(postData);
            $.ajax({
                url: app_url+'crm/sales/create',
                type: 'POST',
                data: postData,
                dataType:'text',
                success: function(data){
                 $("#response .message").html("<strong>Success</strong>: Sale has been created successfully!");
                    $("#response").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                },
                error: function(data){

                  $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                }

            });
        }

    }

    function actionEditSale(){

        var errorCounter = validateForm();

        if (errorCounter > 0) {
            $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#response .message").html("<strong>Error</strong>: It appear's you have forgotten to complete something!");
            $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);

        } else {

            var $btn = $("#action_edit_sale").button("loading");
            $(".required").parent().removeClass("has-error");
            $("#edit_sale").find(':input:disabled').removeAttr('disabled');
            var formData = ($('#edit_sale').serialize());
            var postData = formData;
          //  alert(postData);
            $.ajax({
                url: app_url+'crm/sales/view',
                type: 'POST',
                data: postData,
                dataType:'text',
                success: function(data){
                    $("#response .message").html("<strong>Success</strong>: Sale has been edited successfully!");
                    $("#response").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                },
                error: function(data){

                    $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                }

            });
        }

    }


    function updateInvoice() {

        var $btn = $("#action_update_invoice").button("loading");
        $("#update_invoice").find(':input:disabled').removeAttr('disabled');

        jQuery.ajax({

            url: 'response.php',
            type: 'POST',
            data: $("#update_invoice").serialize(),
            dataType: 'json',
            success: function(data){
                $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#response").removeClass("alert-warning").addClass("alert-success").fadeIn();
                $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                $btn.button("reset");
            },
            error: function(data){
                $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
                $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                $btn.button("reset");
            }
        });

    }

    function downloadCSV(action) {

        jQuery.ajax({

            url: 'response.php',
            type: 'POST',
            data: action,
            dataType: 'json',
            success: function(data){
                $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#response").removeClass("alert-warning").addClass("alert-success").fadeIn();
                $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
            },
            error: function(data){
                $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
                $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
            }
        });

    }

    function actionDeleteRow(meza,rowid){
            alert(meza);
           /* $.ajax({
                url: app_url+'crm/sales/create',
                type: 'POST',
                data: postData,
                dataType:'text',
                success: function(data){
                    $("#response .message").html("<strong>Success</strong>: Sale has been created successfully!");
                    $("#response").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                },
                error: function(data){

                    $("#response .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#response").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({ scrollTop: $('#response').offset().top }, 1000);
                    $btn.button("reset");
                }

            });
*/

    }

    function validateForm() {
        // error handling
        var errorCounter = 0;

        $(".required").each(function(i, obj) {

            if($(this).val() === ''){
                $(this).parent().addClass("has-error");
                errorCounter++;
            } else{
                $(this).parent().removeClass("has-error");
            }


        });

        return errorCounter;
    }

});